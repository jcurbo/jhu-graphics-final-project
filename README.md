# Final project for JHU EP Computer Graphics

James Curbo <jcurbo@pobox.com>

This is my final project for Computer Graphics at Johns Hopkins University - Engineering for Professionals.

This is a Solar System simulator written in C++ and using OpenGL.  It uses orbital mechanics calculations and real observation data to generate a (mostly) realistic model of the Solar System.

More information is available in the included [presentation](https://bitbucket.org/jcurbo/jhu-graphics-final-project/src/e43c9442449338a58e4af77903d502ad1cf2000e/Curbo%20-%20Computer%20Graphics%20Final%20Project.pptx?at=master).

# Software used
- SFML (Simple & Fast Multimedia Layer) http://www.sfml-dev.org/
- SOIL_ext (Simple OpenGL Image Library, extended version) https://github.com/fenbf/SOIL_ext
- GLEW (GL Extension Library) http://glew.sourceforge.net/
- RapidJSON https://github.com/miloyip/rapidjson

# License
See LICENSE file.