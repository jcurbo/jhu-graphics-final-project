#pragma once

#include <cmath>

#include <geometry\geometry.h>
#include <Scene\Scene.h>

class Washer : public TriSurface
{
public:
	Washer(const float r1, const float r2, const unsigned int nSlices, 
		const int positionLoc, const int normalLoc, const int textureLoc)
	{
		float theta = 0;
		float dtheta = (2.0f * (float)M_PI) / (float)nSlices;

		m_nRows = nSlices + 1;

		float cosTheta, sinTheta;

		VertexAndNormal vtx1, vtx2;
		Vector3 n = Vector3(0,0,1);

		for (unsigned int i = 0; i<nSlices; i++, theta += dtheta)
		{
			cosTheta = cosf(theta);
			sinTheta = sinf(theta);

			vtx1.m_vertex.Set(cosTheta * r1,
							 sinTheta * r1,
							 0);
			vtx2.m_vertex.Set(cosTheta * r2,
							 sinTheta * r2,
							 0);

			vtx1.m_normal = n;
			vtx2.m_normal = n;

			m_vertexList.push_back(vtx1);
			m_vertexList.push_back(vtx2);

			//float u = i / (float)nSlices;

			Point2 tex1 = Point2(0, i);
			Point2 tex2 = Point2(1, i);

			m_texCoordList.push_back(tex1);
			m_texCoordList.push_back(tex2);

		}

		// create indices

		m_nCols = 2;

		
		for (unsigned int row = 0; row <= m_nRows; row++) {
			
				m_faceList.push_back(getIndex(row, 0));
				m_faceList.push_back(getIndex(row, 1));
				m_faceList.push_back(getIndex(row+1, 1));
				
				m_faceList.push_back(getIndex(row, 0));
				m_faceList.push_back(getIndex(row+1, 1));
				m_faceList.push_back(getIndex(row+1, 0));
			
		}

		// Connect end
		m_faceList.push_back(getIndex(nSlices - 1, 0));
		m_faceList.push_back(getIndex(nSlices - 1, 1));
		m_faceList.push_back(getIndex(0, 1));

		m_faceList.push_back(getIndex(0, 1));
		m_faceList.push_back(getIndex(0, 0));
		m_faceList.push_back(getIndex(nSlices - 1, 0));

      // Create vertex buffers
      CreateVertexBuffers(positionLoc, normalLoc);
	  CreateTextureBuffers(textureLoc);

	}

private:
	unsigned int m_nRows;
	unsigned int m_nCols;

	// Convenience method to get the index into the vertex list given the
	// "row" and "column" of the subdivision/grid
	unsigned int getIndex(unsigned int row, unsigned int col) const
	{
		//return (col*m_nRows) + row;
		return (row * m_nCols) + col;
	}

	// texture stuff

	GLuint m_texBuffer;
	
	std::vector<Point2> m_texCoordList;

	void CreateTextureBuffers(const int textureCoordLoc)
	{
		m_texBuffer = 0;

		glGenBuffers(1, &m_texBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_texBuffer);
		glBufferData(GL_ARRAY_BUFFER, m_texCoordList.size() * sizeof(Point2),
					m_texCoordList.data(), GL_STATIC_DRAW);

		glBindVertexArray(m_vao);
		glVertexAttribPointer(textureCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(textureCoordLoc);

		glBindVertexArray(0);
		glBindBuffer( GL_ARRAY_BUFFER, 0 );
	}

};