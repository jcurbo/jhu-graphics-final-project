#pragma once

#include <Scene\Scene.h>

#include "Orbit.h"

class PlanetNode : public TransformNode
{
public:
	PlanetNode(OrbitalElements orbit)
	{
		oe = orbit;
		curRotateAngle = 0;
	}

	void updateRotation()
	{
		double fac = 60 * 60 * 60 / 1000;
		double rotPerHr = 360.0f / oe.rotatePeriod;
		double rotPerFrame = rotPerHr / fac;
		curRotateAngle = fmod(curRotateAngle + rotPerFrame, 360);
	}

	void Update(SceneState& sceneState)
	{
		m_matrix.SetIdentity();

		// size
		float r = ((float)oe.radius) / globalScaleFac;
		m_matrix.Scale(r, r, r);

		// axial tilt
		m_matrix.Rotate((float)oe.obliquity,1,0,0);

		// rotation
		m_matrix.Rotate((float)curRotateAngle, 0, 0, 1);
		updateRotation();

		SceneNode::Update(sceneState);
	}

private:
	OrbitalElements oe;
	double curRotateAngle;
};