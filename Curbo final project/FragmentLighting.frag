#version 150

out vec4 fragColor;

in vec3 vertexPositionFrag;
in vec3 vertexNormalFrag;

// Global camera position
uniform vec3  cameraPosition;

// Uniforms for matrices
uniform mat4 pvm;		// Composite projection, view, model matrix
uniform mat4 modelMatrix;	// Modeling  matrix
uniform mat4 normalMatrix;	// Normal transformation matrix

// Uniforms for material properties
uniform vec4   materialAmbient;
uniform vec4   materialDiffuse;
uniform vec4   materialSpecular;
uniform vec4   materialEmission;
uniform float  materialShininess;

// Global lighting environment ambient intensity
uniform vec4  globalLightAmbient;

// Uniform to constrian the number of lights the application uses
uniform int numLights;

// Structure for a light source. Allow up to 8 lights. No attenuation or spotlight
// support yet.
const int MAX_LIGHTS = 8; 
struct LightSource
{
	int  enabled;
	vec4 position;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float constAtt;
	float linAtt;
	float quadAtt;
};
uniform LightSource lights[MAX_LIGHTS]; 

uniform float spotCutoffAngle;
uniform float spotExp;
uniform vec3 spotDir;

in vec2 UV;
uniform sampler2D textureData;
uniform sampler2D textureBumpData;
uniform sampler2D textureSpecData;

// Convenience method to compute the ambient, diffuse, and specular
// contribution of directional light source i
void directionalLight(in int i, in vec3 N, in vec3 vtx, in vec3 V, in float shininess,
                      inout vec4 ambient, inout vec4 diffuse, 
                      inout vec4 specular)
{

   
   // Normalize the light direction. Note: we could use a varying parameter 
   // to pass in the normalized light source direction. However, the cost of
   // doing the perspective correct interpolation may be higher than just
   // getting the vector here.
   vec3 L = lights[i].position.xyz;
   float dist = length(L);
   L = normalize(L);
 

	// Add light source ambient
   ambient += lights[i].ambient;
   
   // Dot product of normal and light direction
   float nDotL = dot(N, L);
   if (nDotL > 0.0)
   {
      // Add light source diffuse modulated by NDotL
      diffuse += lights[i].diffuse * nDotL; 
      
      // Construct the halfway vector - note that we are assuming local viewpoint
      vec3 H = normalize(L + V);
      
      // Add specular contribution (if N dot H > 0)
      float nDotH = dot(N, H);
      if (nDotH > 0.0)
         specular += lights[i].specular * pow(nDotH, shininess);
   }
}

// Convenience method to compute the ambient, diffuse, and specular
// contribution of point light source i
void pointLight(in int i, in vec3 N, in vec3 vtx, in vec3 V, in float shininess,
                inout vec4 ambient, inout vec4 diffuse, inout vec4 specular)
{
   // Construct a vector from the vertex to the light source. Find the length for
   // use in attenuation. Normalize that vector (L)
   // distance 
   vec3 L = lights[i].position.xyz - vtx;
   float dist = length(L);
   L = normalize(L);
   

   // compute attenuation
   float att = 1.0 / (lights[i].constAtt +
				lights[i].linAtt * dist + 
				lights[i].quadAtt * dist * dist);

   // Add the light source ambient contribution
   ambient += lights[i].ambient * att;
   
   // Determine dot product of normal with L. If < 0 the light is not 
   // incident on the front face of the surface.
   float nDotL = dot(N, L);
   if (nDotL > 0.0)
   {
      // Add diffuse contribution of this light source
      diffuse  += lights[i].diffuse * nDotL * att;
      
      // Construct the halfway vector
      vec3 H = normalize(L + V);
      
      // Add specular contribution (if N dot H > 0)
      float nDotH = dot(N, H);
      if (nDotH > 0.0)
         specular += lights[i].specular * pow(nDotH, shininess) * att;

/**    
      // Lets use the reflection vector!
      vec3 R = reflect(-L, N);
      
      // Add specular contribution (if R dot V > 0)
      float rDotV = dot(R, V);
      if (rDotV > 0.0)
         specular += lights[i].specular * attenuation * pow(rDotV, shininess);
**/
    }
}

void spotLight(in int i, in vec3 N, in vec3 vtx, in vec3 V, in float shininess,
                inout vec4 ambient, inout vec4 diffuse, inout vec4 specular,
				in float spotCutoffAngle, in float spotExp, in vec3 spotDir)
{
	float spotFactor, att, dist;

	// Vector from vertex to light
	vec3 L = lights[i].position.xyz - vtx;
	dist = length(L);
	L = normalize(L);

	// compute Lambert term
	float NdotL = max(dot(N, L), 0.0);

	if (NdotL > 0.0) {
		
		// diffuse and specular lighting can happen, now let's see if we're
		// in the cone of the spotlight

		if (dot(-spotDir, L) > cos(radians(spotCutoffAngle))) {

			spotFactor = pow(dot(-spotDir, L), spotExp);
			att = spotFactor / (lights[i].constAtt +
								lights[i].linAtt * dist + 
								lights[i].quadAtt * dist * dist);

			// Add diffuse contribution of this light source
			diffuse += att * (lights[i].diffuse * NdotL);
      
			// Construct the halfway vector
			vec3 H = normalize(L + V);
      
			// Add specular contribution (if N dot H > 0)
			float nDotH = dot(N, H);
		    if (nDotH > 0.0) {
				specular += att * lights[i].specular * pow(nDotH, shininess);
			}
	    }
	}
}

// another attempt at normal mapping via 
// http://www.gamasutra.com/blogs/RobertBasler/20131122/205462/Three_Normal_Mapping_Techniques_Explained_For_the_Mathematically_Uninclined.php?print=1
// "Followup: Normal Mapping Without Precomputed Tangents" from http://www.thetenthplanet.de/archives/1180
mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv )
{
	/* get edge vectors of the pixel triangle */
	vec3 dp1 = dFdx( p );
	vec3 dp2 = dFdy( p );
	vec2 duv1 = dFdx( uv );
	vec2 duv2 = dFdy( uv );

	/* solve the linear system */
	vec3 dp2perp = cross( dp2, N );
	vec3 dp1perp = cross( N, dp1 );
	vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
	vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

	/* construct a scale-invariant frame */
	float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
	return mat3( T * invmax, B * invmax, N );
}

vec3 perturb_normal( vec3 N, vec3 V, vec2 texcoord )
{
	/* assume N, the interpolated vertex normal and V, the view vector (vertex to eye) */
	vec3 map = texture2D( textureBumpData, texcoord ).xyz;
	// WITH_NORMALMAP_UNSIGNED
	map = map * 255./127. - 128./127.;
	// WITH_NORMALMAP_2CHANNEL
	// map.z = sqrt( 1. - dot( map.xy, map.xy ) );
	// WITH_NORMALMAP_GREEN_UP
	// map.y = -map.y;
	mat3 TBN = cotangent_frame( N, -V, texcoord );
	return normalize( TBN * map );
}


void main()
{

	// Transform normal and position to world coords. 
	vec3 N = normalize(vec3(normalMatrix * vec4(vertexNormalFrag, 0.0)));
	vec3 v = vec3((modelMatrix * vec4(vertexPositionFrag, 1.0)));
	
	// Construct a unit length vector from the vertex to the camera  
	vec3 V = normalize(cameraPosition - v);

	// normal mapping
	float maxVar = 2.0;
	float minVar = maxVar / 2.0;
	//vec3 normalMap = N + normalize(texture2D(textureBumpData, UV).rgb * maxVar - minVar); 
	//vec3 normalMap = perturb_normal(N, V, UV);
	vec3 normalMap = N;

	// specular map from texture
	vec3 specMap = texture2D(textureSpecData, UV).rgb;
	
	// Iterate through all lights to determine the illumination striking this vertex
	vec4 ambient  = vec4(0.0);
	vec4 diffuse  = vec4(0.0);
	vec4 specular = vec4(0.0);
	for (int i = 0; i < numLights; i++)
	{
		if (lights[i].enabled == 1)
		{
			if (lights[i].position.w == 0.0) {
				directionalLight(i, normalMap, v, V, materialShininess, ambient, diffuse, specular);
			} else if (lights[i].position.w == 1.0) {
				pointLight(i, normalMap, v, V, materialShininess, ambient, diffuse, specular);
			} else if (lights[i].position.w == 2.0) {
				spotLight(i, normalMap, v, V, materialShininess, ambient, diffuse, specular,
							spotCutoffAngle, spotExp, spotDir);
			}
			
		}
	}

	// Compute color. Emmission + global ambient contribution + light sources ambient, diffuse,
	// and specular contributions
	vec3 lightColor = globalLightAmbient.rgb * materialAmbient.rgb 
						+ ambient.rgb * materialAmbient.rgb
						+ diffuse.rgb * materialDiffuse.rgb
						+ specular.rgb * materialSpecular.rgb * specMap;
	vec3 lightTexColor = materialEmission.rgb + lightColor * texture2D(textureData, UV).rgb;
	fragColor = vec4(lightTexColor, texture2D(textureData, UV).a);
	

}
