//============================================================================
//	Johns Hopkins University Engineering Programs for Professionals
//	605.467 Computer Graphics and 605.767 Applied Computer Graphics
//	Instructor:	David W. Nesbitt
//
//	Author:	David W. Nesbitt
//	File:    LineShaderNode.h
//	Purpose:	Derived class to handle an offset line shader and its uniforms and 
//          attribute locations.
//
//============================================================================

#ifndef __LINESHADERNODE_H
#define __LINESHADERNODE_H

#include <vector>
#include "scene/scene.h"

#include <GL/glew.h>
#include <GL/GL.h>

/**
 * Offset line shader node.
 */
class LineShaderNode: public ShaderNode
{
public:
   /**
    * Gets uniform and attribute locations.
    */
   bool GetLocations() 
   {
      m_positionLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "position");
      if (m_positionLoc < 0)
      {
         printf("Error getting vertex position location\n");
         return false;
      }
      m_colorLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "color");
      if (m_colorLoc < 0)
      {
         printf("Error getting color location\n");
         //return false;
      }

	  // Populate matrix uniform locations in scene state
      m_pvmLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "pvm");
      m_modelMatrixLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "modelMatrix");

      return true;
   }

	/**
    * Draw method for this shader - enable the program and set up uniforms
    * and vertex attribute locations
    * @param  sceneState   Current scene state.
	 */
	virtual void Draw(SceneState& sceneState)
	{
      // Enable this program
      m_shaderProgram.Use();

      // Set scene state locations to ones needed for this program
      sceneState.m_positionLoc    = m_positionLoc;
      sceneState.m_colorLoc       = m_colorLoc;
      sceneState.m_pvmLoc = m_pvmLoc;
      sceneState.m_modelMatrixLoc = m_modelMatrixLoc;

      // Draw all children
		SceneNode::Draw(sceneState);
	}

	// Returns position attribute location
	int GetPositionLoc() const
	{
		return m_positionLoc;
	}

	// Returns color attribute location
	int GetColorLoc() const
	{
		return m_colorLoc;
	}

protected:
   // Uniform and attribute locations
   GLint m_colorLoc;
   GLint m_positionLoc;
   GLint m_pvmLoc;
   GLint m_modelMatrixLoc;
};

#endif