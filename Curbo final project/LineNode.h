//============================================================================
//	Johns Hopkins University Engineering Programs for Professionals
//	605.467 Computer Graphics and 605.767 Applied Computer Graphics
//	Instructor:	David W. Nesbitt
//
//	Author:	David W. Nesbitt
//	File:    LineNode.h
//	Purpose:	Simple geometry node that draws a connected line.
//
//============================================================================

// Modified by James Curbo for Fall 2014 Computer Graphics class

#ifndef __LINENODE_H
#define __LINENODE_H

#include <vector>
#include "scene/scene.h"

#include <stdlib.h>
#include <time.h>

/**
 * Unit sphere geometry node.
 */
class LineNode: public GeometryNode
{
public:
   /**
    * Constructor.
    */
	LineNode(const int positionLoc, const int colorLoc)
	{
		// Maximum number of lines that can be drawn.  Wanted to set globally but would have to
		// modify GeometryNode so just left it statically determined
		numLines = 250;

		// Uses numLines to reserve capacity in these vectors (doesn't change number of items)
		// note that a color is stored for each vertex, so if you want a line to be one color,
		// you have to store the same color twice
		pts.reserve(numLines*2);
		colors.reserve(numLines*2);
		
	  // This holds the current number of vertices in the object
	  m_vertexCount = 0;

	  // Create VBOs, one for vertices, one for colors
	  glGenBuffers(1, &m_vbo);
	  glGenBuffers(1, &m_vbo_c);

	  // Activates the VBO for vertex data
	  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	  // Loads the vertices into the VBO
	  // args: target, size, pointer to beginning of data, type of storage
	  glBufferData(GL_ARRAY_BUFFER, numLines * 2 * sizeof(Point2),  (GLvoid*)&pts[0], GL_DYNAMIC_DRAW);

	  // Activates the VBO for colors
	  glBindBuffer(GL_ARRAY_BUFFER, m_vbo_c);
	  glBufferData(GL_ARRAY_BUFFER, numLines * 2 * sizeof(Color4), (GLvoid*)&colors[0], GL_DYNAMIC_DRAW);


	  // Allocate a VAO and activate it
	  glGenVertexArrays(1, &m_vao);
	  glBindVertexArray(m_vao);

	  // Set position attribute for VAO
	  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	  glEnableVertexAttribArray(positionLoc);
	  glVertexAttribPointer(positionLoc, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	  // set color attribute for VAO
	  glBindBuffer(GL_ARRAY_BUFFER, m_vbo_c);
	  glEnableVertexAttribArray(colorLoc);
	  glVertexAttribPointer(colorLoc, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
	
   /**
    * Destructor
    */
	virtual ~LineNode() 
	{
		// deletes the VBOs and VAO
		glDeleteBuffers(1, &m_vbo);
		glDeleteBuffers(1, &m_vbo_c);
		glDeleteVertexArrays(1, &m_vao);
	
	}

	/**
	 * Draw the lines
    * @param  sceneState  Current scene state.
	 */
	virtual void Draw(SceneState& sceneState)
	{

		// bind the VAO
		glBindVertexArray(m_vao);

		// We've got two groups - all the older lines, and the current line
		// We need to draw them with different line widths

		// Draw "older" lines (all but the last line, aka last two vertices)
		glLineWidth(2.0);
		glDrawArrays(GL_LINES, 0, m_vertexCount-2);

		// Draw "current" line
		glLineWidth(4.0);
		glDrawArrays(GL_LINES, m_vertexCount-2, 2);

		// unbind VAO
		glBindVertexArray(0);

	}

	// Main function that adds a line to this object
	void addLine(Point2 begin, Point2 end)
	{
		// Adds points to point list
		pts.push_back(begin);
		pts.push_back(end);

		// Add line segment to segment list
		segments.push_back(LineSegment2(begin, end));

		// the last line (aka the current line) is always red & green
		colors.push_back(Color4(1.0f, 0, 0, 1.0f));
		colors.push_back(Color4(0, 1.0f, 0, 1.0f));	

		// update VBO and state
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		// using m_vertexCount gives us a way to index directly to the end of the actual data in the VBO
		// the pts vector is already 2 Point2's bigger, so the data pointer will point at the last two items
		glBufferSubData(GL_ARRAY_BUFFER, m_vertexCount * sizeof(Point2), 2 * sizeof(Point2), (GLvoid*)&pts[m_vertexCount]);
		
		// Do the same for the colors
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_c);
		glBufferSubData(GL_ARRAY_BUFFER, m_vertexCount * sizeof(Color4), 2 * sizeof(Color4), (GLvoid*)&colors[m_vertexCount]);

		// increment the amount of vertices since we're done
		m_vertexCount += 2;
	}

	// Modifies the last vertex in the line and updates the VBO
	void modifyLastVertex(Point2 pt)
	{
		// update list of points by overwriting last point
		pts.back() = pt;
		// update list of segments by overwriting last segment
		segments.back() = LineSegment2(pts.rbegin()[1], pts.rbegin()[0]);
		// finds list of intersects with "current" line
		//findCurrentLineIntersects();

		// update VBO and state for vertices
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		// we provide an index to the last live item in the VBO and array
		glBufferSubData(GL_ARRAY_BUFFER, (m_vertexCount - 1) * sizeof(Point2), sizeof(Point2), (GLvoid*)&pts[m_vertexCount-1]);

		// Note we do not change the color here; since we're modifying the "current" line it stays red and green.
	}

	// Provides the random color when a line is "dropped" (aka, moved from current to older)
	// This is called when the mouse button is released to update the color vector
	void modifyLastColor()
	{
		// Generate a random color and set it
		Color4 r = randColor();
		colors[m_vertexCount - 1] = r;
		colors[m_vertexCount - 2] = r;

		// Update the color VBO
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_c);
		glBufferSubData(GL_ARRAY_BUFFER, (m_vertexCount - 2) * sizeof(Color4), 2 * sizeof(Color4), (GLvoid*)&colors[m_vertexCount-2]);
	}

	// generates a random color
	Color4 randColor()
	{
		Color4 color;
		float rr, rg, rb;

		// do/while so we generate some colors and then check to make sure they meet requirements
		do {
			rr = rand01();
			rg = rand01();
			rb = rand01();
		} while (rr >= 0.75 && rg >= 0.75 && rb >= 0.75);

		return Color4(rr, rg, rb, 1.0f);
	}

	// This clears out the vertex and color data (used when 'c' is pressed)
	void clearPts()
	{
		// clear vectors of all data
		pts.clear();
		colors.clear();
		// since clear removes all objects, resize the vectors back to 500 vertices 
		pts.reserve(numLines*2);
		colors.reserve(numLines*2);

		// Update VBOs for vertices and colors
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numLines * 2 * sizeof(Point2), (GLvoid*)&pts[0]);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_c);
		glBufferSubData(GL_ARRAY_BUFFER, 0, numLines * 2 * sizeof(Color4), (GLvoid*)&colors[0]);

		// Update vertex count
		m_vertexCount = 0;
	}

	// Finds any intersection between the last line in the segments array and
	// all other current lines
	// This function gets used updating the "points" object, which holds all points associated with
	// older lines
	// It returns the list of intersection points for the newest one of the "older" lines
	std::vector<Point2> findLastLineIntersects()
	{
		std::vector<Point2> newPts;
		// If there is only one line, segments.size()-1 = 0 and the for loop
		// will not fire (nothing to do)
		for (size_t i=0; i<segments.size()-1; i++) {
			Point2 iPt;
			if (segments.back().Intersect(segments[i], iPt)) {
				newPts.push_back(iPt);
			}
		}
		return newPts;
	}

	// This finds intersections on the "current" line
	// This is used to update "points_cur", which holds all intersections associated 
	// with the "current" line
	// It returns the list of intersection points for that line
	std::vector<Point2> findCurrentLineIntersects()
	{
		std::vector<Point2> newPts;
		for (size_t i=0; i<segments.size()-1; i++) {
			Point2 iPt;
			if (segments.back().Intersect(segments[i], iPt)) {
				newPts.push_back(iPt);
			}
		}
		return newPts;
	}

	// Getter for numLines (max possible lines)
	unsigned int getMaxLines()
	{
		return numLines;
	}

	// Getter for current number of lines
	unsigned int getLines()
	{
		return m_vertexCount/2;
	}

	// Prints a list of all segments
	void printSegments()
	{
		printf("current segments array: ");
		for (size_t i=0; i<segments.size(); i++)
			printf("  [(%.2f, %.2f) -> (%.2f, %.2f)]\n", 
				segments[i].A.x, segments[i].A.y,
				segments[i].B.x, segments[i].B.y);
		printf("\n");
	}

	// Prints a list of all points
	void printPts()
	{
		printf("current point array: ");
		for (size_t i=0; i<pts.size(); i++)
			printf("(%.0f, %.0f), ", pts[i].x, pts[i].y);
		printf("\n");
	}

	// Prints a list of all colors
	void printColors()
	{
		printf("current colors array: ");
		for (size_t i=0; i<colors.size(); i++)
			printf("(%.2f, %.2f, %.2f), ", colors[i].r, colors[i].g, colors[i].b);
		printf("\n");
	}


protected:
	unsigned int m_vertexCount; // number of vertices
	GLuint m_vbo;				// vector buffer object
	GLuint m_vbo_c;				// VBO for colors
	GLuint m_vao;				// vertex array object
	Color4 m_color;				// color for lines
	std::vector<Point2> pts;		// list of vertices
	std::vector<Color4> colors;		// list of colors
	unsigned int numLines;			// max number of lines
	std::vector<LineSegment2> segments;	 // list of line segments
};

#endif