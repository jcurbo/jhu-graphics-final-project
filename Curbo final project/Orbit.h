#pragma once

#include <cmath>

struct OrbitalElements
{
	std::string name;		// name of object
	unsigned int id;		// id from NASA HORIZONS
	double epoch;			// Julian day of elements
	double ecc;				// eccentricity of orbit
	double distPeri;		// distance from parent at periapsis (km)
	double incl;			// inclination of orbit (deg)
	double longAscNode;		// longitude of ascending node (deg)
	double argPeri;			// argument of periapsis (deg)
	double timePeri;		// time of periapsis passage (day)
	double meanMotion;		// motion per day along orbit (deg)
	double meanAnomaly;		// mean anomaly (deg)
	double trueAnomaly;		// initial value of true anomaly (deg)
	double semiMajorAxis;	// length of semi-major axis (km)
	double distApo;			// distance at apoapsis (km)
	double period;			// orbital period (days)
	double curTrueAnomaly;  // current value of true anomaly (used in updates) (deg)
	double radius;			// radius of object (km)
	double obliquity;		// axial tilt (obliquity) of object (deg)
	double rotatePeriod;	// rotational period (hours)
	std::string parent;		// parent body (string)
};

double deg2rad(double deg) { return (M_PI/180) * deg; }
double rad2deg(double rad) { return (180/M_PI) * rad; }

// Calculates the current radius of the orbit (distance from the sun) based on this formula:
// a = semi major axis of the ellipse (semiMajorAxis)
// e = eccentricity of the ellipse (ecc)
// v = current value of the true anomaly of the orbit (curTrueAnomaly)
//
//       a * (1-e^2) 
// r = ---------------
//     1 + (e * cos v)
double calcRadialDistance(double a, double e, double v)
{
	return (a * (1 - pow(e, 2)) / (1+(e * cos(deg2rad(v)))));
}

// Calculates x,y,z Cartesian coordinates based on the Sun as origin
//
// a = semimajor axis (AU)
// e = eccentricity (degrees)
// o = longitude of ascending node (degrees)
// w = argument of perhelion (degrees)
// i = inclination (degrees)
// v = true anomaly (angle between perhelion and current position) (degrees)
// r = radius (computed by calcRadialDistance()
//
// x = r * (cos o * cos (w + v) - (sin o * cos i * sin(w + v)))
// y = r * (sin o * cos (w + v) + (cos o * cos i * sin(w + v)))
// z = r * (sin i * sin (w + v))
Point3 calcHelioCoords(double a, double e, double o, double w, double i, double v)
{
	Point3 pos;
	double r = calcRadialDistance(a, e, v);

	/*printf("helio coord input numbers: a %0.2f\n", a);
	printf("                           e %0.2f\n", e);
	printf("                           o %0.2f\n", o);
	printf("                           w %0.2f\n", w);
	printf("                           i %0.2f\n", i);
	printf("                           v %0.2f\n", v);
	printf("                           r %0.2f\n", r);*/

	pos.x = (float)(r * (cos(deg2rad(o)) * cos(deg2rad(w + v))
		- ((sin(deg2rad(o)) * cos(deg2rad(i)) * sin(deg2rad(w + v))))));
	pos.y = (float)(r * (sin(deg2rad(o)) * cos(deg2rad(w + v))
		+ ((cos(deg2rad(o)) * cos(deg2rad(i)) * sin(deg2rad(w + v))))));
	pos.z = (float)(r * (sin(deg2rad(i)) * sin(deg2rad(w + v))));

	/*printf("helio calculated coords: x %0.2f\n", pos.x);
	printf("                         y %0.2f\n", pos.y);
	printf("                         z %0.2f\n", pos.z);*/

	return pos;
}

