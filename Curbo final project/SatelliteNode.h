#pragma once

#include <Scene\Scene.h>
#include "OrbitNode.h"

extern std::map<std::string, OrbitNode*> orbitNodes;

class SatelliteNode : public TransformNode
{
public:
	SatelliteNode(std::string parentBody)
	{
		parent = parentBody;
	}

	void Update(SceneState& sceneState)
	{
		LoadIdentity();

		parentPos = orbitNodes[parent]->getCurrentPosition();
		Translate(parentPos.x, parentPos.y, parentPos.z);

		SceneNode::Update(sceneState);

	}

private:
	Point3 parentPos;
	std::string parent;
};