#pragma once

#include "Scene\Scene.h"
#include "GL\glew.h"
//#include <GL\GL.h>

class SkyNode : public PresentationNode
{
public:
	SkyNode ()
	{
	}

	void Draw(SceneState& sceneState)
	{
		//glClear(GL_DEPTH_BUFFER_BIT);
		//glDisable(GL_DEPTH_TEST);
		//glDisable(GL_LIGHTING);

		SceneNode::Draw(sceneState);

		//glEnable(GL_DEPTH_TEST);
		//glEnable(GL_LIGHTING);
	}

};