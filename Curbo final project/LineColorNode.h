//============================================================================
//	Johns Hopkins University Engineering Programs for Professionals
//	605.467 Computer Graphics and 605.767 Applied Computer Graphics
//	Instructor:	David W. Nesbitt
//
//	Author:	David W. Nesbitt
//	File:    ColorNode.h
//	Purpose:	Simple presentation node that defines a color.
//
//============================================================================

#ifndef __COLORNODE_H
#define __COLORNODE_H

#include <list>
#include "Scene\Scene.h"

#include <GL\glew.h>
#include <GL\GL.h>

/**
 * Color presentation node.
 */
class LineColorNode: public PresentationNode
{
public:

	// Default constructor - picks a random color
   LineColorNode()
   {
	  SeedRandomColors();
	  lineColor = GetRandomColor();
   }

   // Use a particular color
   LineColorNode(Color4 c)
   {
	   lineColor = c;
   }

   /**
    * Destructor
    */
	virtual ~LineColorNode() { }

	/**
	 * Draw this presentation node and its children
	 */
	virtual void Draw(SceneState& sceneState)
	{
      // Set the current color and draw all children. Very simple lighting support
      glUniform4fv(sceneState.m_colorLoc, 1, &lineColor.r);
      SceneNode::Draw(sceneState);
	}

	// Seed the random number generator for colors
	void SeedRandomColors()
	{
	   srand(111000);
	}

	// Get a random color. Make sure it isn't too bright (no component > 0.75)
	Color4 GetRandomColor()
	{
	   float r = 1.0f;
	   float g = 1.0f;
	   float b = 1.0f;
	   while(r >= 0.75 && g >= 0.75 && b >= 0.75)
	   {
		  r = rand01();
		  g = rand01();
		  b = rand01();
	   }
	   return Color4(r, g, b, 1.0f);
	}

protected:
   Color4 lineColor;
};

#endif
