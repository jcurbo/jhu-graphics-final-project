#version 150


// Per vertex lighting - vertex shader. Outputs a varying (intepolated) color to the 
// fragment shader
//smooth out vec4 color;

// Incoming vertex and normal attributes
in vec3 vertexPosition;		// Vertex position attribute
in vec3 vertexNormal;			// Vertex normal attribute
in vec2 vertexUV;

out vec2 UV;

// Send them right back out
out vec3 vertexPositionFrag;
out vec3 vertexNormalFrag;

uniform mat4 pvm;		// Composite projection, view, model matrix

// Simple shader for per vertex lighting.
void main()
{
	
	vertexPositionFrag = vertexPosition;
	vertexNormalFrag = vertexNormal;

	// Convert position to clip coordinates and pass along
	gl_Position = pvm * vec4(vertexPosition, 1.0);

	// pass texture coordinate
	UV = vertexUV;
}
