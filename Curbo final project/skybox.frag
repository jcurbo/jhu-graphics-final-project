#version 150

out vec4 fragColor;

in vec2 UV;
uniform sampler2D textureData;

void main()
{
	fragColor = texture(textureData, UV);
}