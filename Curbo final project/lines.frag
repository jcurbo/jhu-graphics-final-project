// Simple fragment shader that sets the pixel color to the color specified via 
// the color uniform variable
#version 150
in vec4 colorV;
out vec4 fragColor;       // Output color to the frame buffer

void main() 
{
	// Uses the color passed in from the vertex shader
    fragColor = colorV;
}