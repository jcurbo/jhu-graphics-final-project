#pragma once

#include "OrbitNode.h"

class SunNode : public OrbitNode
{
public:
	SunNode(OrbitalElements orbit) : OrbitNode()
	{
		printf("sunnode constructor called\n");
		oe = orbit;
		currentPosition = Point3(0,0,0);

		objPositions[oe.name] = currentPosition;
	}

	void Update(SceneState& sceneState)
	{
		SceneNode::Update(sceneState);
	}

};