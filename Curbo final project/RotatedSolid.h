#pragma once

#include <cmath>

#include <geometry\geometry.h>
#include <Scene\Scene.h>

#include "GL/glew.h"

class RotatedSolid : public TriSurface
{
public:
	RotatedSolid (const unsigned int nSlices, 
		const int positionLoc, const int normalLoc, const int textureLoc)
	{
		float theta = 0;
		float dtheta = (2.0f * (float)M_PI) / (float)nSlices;
		float cosTheta, sinTheta;
		
		const unsigned int nStacks = 5; // number of vertices in our model
		m_nRows = nStacks;
		m_nCols = nSlices;

		VertexAndNormal v2,v3,v4,v5,v6;
		Vector3 n;

		// we've just unrolled the inner loop since they're all different
		for (unsigned int i = 0; i < nSlices; i++, theta += dtheta)
		{
			cosTheta = cosf(theta);
			sinTheta = sinf(theta);

			// These normals are just guesses really, proper normals
			// would be easier to generate in a modeling program
			v2.m_vertex.Set(cosTheta * 1.5f,
							sinTheta * 1.5f,
							0.0f);
			v2.m_normal = v2.m_vertex - Point3(0,0,(float)i);
			v3.m_vertex.Set(cosTheta * 0.5f,
							sinTheta * 0.5f,
							2.0f);
			v3.m_normal = v3.m_vertex - Point3(0,0,(float)i);
			v4.m_vertex.Set(cosTheta * 2.0f,
							sinTheta * 2.0f,
							2.0f);
			v4.m_normal = v4.m_vertex - Point3(0,0,(float)i);
			v5.m_vertex.Set(cosTheta * 2.0f,
							sinTheta * 2.0f,
							5.0f);
			v5.m_normal = v5.m_vertex - Point3(0,0,(float)i);
			v6.m_vertex.Set(0.0f,0.0f,7.0f);
			v6.m_normal = v6.m_vertex - Point3(0,0,(float)i);

			m_vertexList.push_back(v2);
			m_vertexList.push_back(v3);
			m_vertexList.push_back(v4);
			m_vertexList.push_back(v5);
			m_vertexList.push_back(v6);

			/*printf("\nv2: %0.2f %0.2f %0.2f\n", v2.m_vertex.x, v2.m_vertex.y, v2.m_vertex.z);
			printf("v3: %0.2f %0.2f %0.2f\n", v3.m_vertex.x, v3.m_vertex.y, v3.m_vertex.z);
			printf("v4: %0.2f %0.2f %0.2f\n", v4.m_vertex.x, v4.m_vertex.y, v4.m_vertex.z);
			printf("v5: %0.2f %0.2f %0.2f\n", v5.m_vertex.x, v5.m_vertex.y, v5.m_vertex.z);
			printf("v6: %0.2f %0.2f %0.2f\n", v6.m_vertex.x, v6.m_vertex.y, v6.m_vertex.z);*/

			// texture coordinate generation
			float u = i / (float)nSlices;

			m_texCoordList.push_back(Point2(u, 1.0f));
			m_texCoordList.push_back(Point2(u, 0.75f));
			m_texCoordList.push_back(Point2(u, 0.5f));
			m_texCoordList.push_back(Point2(u, 0.25f));
			m_texCoordList.push_back(Point2(u, 0.0f));
		}

		for (unsigned int i = 0; i < nStacks; i++)
		{
			for (unsigned int j = 0; j < nSlices; j++)
			{
				m_faceList.push_back(getIndex(i+1, j));
				m_faceList.push_back(getIndex(i, j));
				m_faceList.push_back(getIndex(i, j+1));
				
				m_faceList.push_back(getIndex(i+1, j));
				m_faceList.push_back(getIndex(i, j+1));
				m_faceList.push_back(getIndex(i+1, j+1));

				/*printf("\ntri 1: %d %d %d\n", getIndex(i+1, j), getIndex(i, j), getIndex(i, j+1));
				printf("tri 2: %d %d %d\n", getIndex(i+1, j), getIndex(i, j+1), getIndex(i+1, j+1));*/

				/*printf("triangle 1: %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i+1, j)].m_vertex.x, m_vertexList[getIndex(i, j)].m_vertex.y, m_vertexList[getIndex(i, j+1)].m_vertex.z);
				printf("            %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i+1, j)].m_vertex.x, m_vertexList[getIndex(i, j)].m_vertex.y, m_vertexList[getIndex(i, j+1)].m_vertex.z);
				printf("            %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i+1, j)].m_vertex.x, m_vertexList[getIndex(i, j)].m_vertex.y, m_vertexList[getIndex(i, j+1)].m_vertex.z);

				printf("triangle 2: %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i+1, j)].m_vertex.x, m_vertexList[getIndex(i, j)].m_vertex.y, m_vertexList[getIndex(i, j+1)].m_vertex.z);
				printf("            %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i, j+1)].m_vertex.x, m_vertexList[getIndex(i, j+1)].m_vertex.y, m_vertexList[getIndex(i, j+1)].m_vertex.z);
				printf("            %0.2f %0.2f %0.2f\n", m_vertexList[getIndex(i+1, j+1)].m_vertex.x, m_vertexList[getIndex(i+1, j+1)].m_vertex.y, m_vertexList[getIndex(i+1, j+1)].m_vertex.z);*/
			}

		}

		// Connect the last column with the first
      for (unsigned int i = 0; i < nStacks; i++)
      {
			m_faceList.push_back(getIndex(i+1, nSlices - 1));
			m_faceList.push_back(getIndex(i,   nSlices - 1));
			m_faceList.push_back(getIndex(i,   0));
				
			m_faceList.push_back(getIndex(i+1, nSlices - 1));
			m_faceList.push_back(getIndex(i,   0));
			m_faceList.push_back(getIndex(i+1, 0));
      }

		CreateVertexBuffers(positionLoc, normalLoc);
		CreateTextureBuffers(textureLoc);

	}

private:
	unsigned int m_nRows;
    unsigned int m_nCols;

	GLuint m_texBuffer;
	
	std::vector<Point2> m_texCoordList;

	void CreateTextureBuffers(const int textureCoordLoc)
	{
		m_texBuffer = 0;

		glGenBuffers(1, &m_texBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_texBuffer);
		glBufferData(GL_ARRAY_BUFFER, m_texCoordList.size() * sizeof(Point2),
					m_texCoordList.data(), GL_STATIC_DRAW);

		glBindVertexArray(m_vao);
		glVertexAttribPointer(textureCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(textureCoordLoc);

		glBindVertexArray(0);
		glBindBuffer( GL_ARRAY_BUFFER, 0 );
	}

	// Convenience method to get the index into the vertex list given the
	// "row" and "column" of the subdivision/grid
	unsigned int getIndex(unsigned int row, unsigned int col) const
	{
		return ((col*m_nRows + row) % (m_nRows*m_nCols));
	}
};
