// OpenGL includes
#include <GL/glew.h>

// SFML includes
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>

// C++ includes
#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <string>

// Scenegraph and local stuff
#include "geometry\geometry.h"
#include "Scene\Scene.h"
#include "OrbitNode.h"
#include "LineShaderNode.h"
#include "LineColorNode.h"
#include "OrbitPathNode.h"
#include "TextureNode.h"
#include "PlanetNode.h"
#include "SatelliteNode.h"
#include "SkyNode.h"
#include "SkyShaderNode.h"
#include "RotatedSolid.h"
#include "washer.h"
#include "SunNode.h"

// Misc
#include "JSONImport.h"

// Global variables

// main window (from SFML)
sf::RenderWindow window;

// scenegraph stuff
SceneState mySceneState;
SceneNode* sceneRoot;
CameraNode* myCamera;
TransformNode* sceneTNode;

// JSON reader that holds orbital element data for system objects
JSONImporter* jsondb;

// default window sizes
const int windowWidth = 1024;
const int windowHeight = 768;

// "current" window sizes
int currentWidth = windowWidth;
int currentHeight = windowHeight;

// Some useful numbers
const float AUtoKM = 149597871.0f;
const float AUtoMM = AUtoKM / 1000;
// used to scale movement/animation (time scaling)
const float globalScaleFac = 1000.0f;

// option flags

// show orbit paths or not?
bool orbits_on = true;
bool captureText = false;

// useful groups of information and nodes across all objects
// list of positions
std::map<std::string, Point3> objPositions;
// List of orbital paths
std::map<std::string, OrbitPathNode*> orbitPathList;
// List of orbit nodes
std::map<std::string, OrbitNode*> orbitNodes;
// List of names from JSON
std::vector<std::string> validObjNames;

//Point3 sceneOrigin = Point3(0,0,0);

//std::string currentCameraTarget;

// Command buffer for F5 command line
std::string commandBuffer;

// Simple logging function
void logmsg(const char *message, ...)
{
   // Open file if not already opened
   static FILE *lfile = NULL;
   if (lfile == NULL)
      lfile = fopen("solarsystemsimulator.log", "w");

   va_list arg;
   va_start(arg, message);
   vfprintf(lfile, message, arg);
   putc('\n', lfile);
   fflush(lfile);
   va_end(arg);
}

void defaultCameraSetup()
{
	assert(orbitNodes.size() > 0); // this needs to be done first

	myCamera->SetPosition(Point3(0, 1*AUtoMM, 0));
	myCamera->SetLookAtPt(Point3(0,0,0));
	myCamera->SetViewUp(Vector3(0,0,1));
	myCamera->SetPerspective(75.0f, (float)currentWidth / (float)currentHeight, 1.0f, 100*AUtoMM);
	myCamera->setCameraMode(FREE);
	myCamera->setCenter(orbitNodes["sun"]);
}

// main scene construction
void ConstructScene()
{
	// Scenegraph root
	sceneRoot = new SceneNode;

	// -------------------------------------------------------------------------------------
	// Camera setup
	myCamera = new CameraNode();
	sceneRoot->AddChild(myCamera);

	// -------------------------------------------------------------------------------------
	// transform node for the entire scene
	sceneTNode = new TransformNode;
	myCamera->AddChild(sceneTNode);



	// -------------------------------------------------------------------------------------
	// skybox
	SkyShaderNode* skyShader = new SkyShaderNode;
	if (!skyShader->Create("skybox.vert", "skybox.frag") ||
		!skyShader->GetLocations())
		exit(-1);

	int skyPositionLoc = skyShader->GetPositionLoc();
	int skyTextureCoordLoc = skyShader->GetTextureCoordLoc();

	TransformNode* skyTNode = new TransformNode;
	TextureNode* skyTexNode = new TextureNode("milkyway-eso-big.jpg", "none", "none");
	StackSphere* skySphere = new StackSphere(100,100, skyPositionLoc, 0, skyTextureCoordLoc);

	skyShader->AddChild(skyTNode);
	skyTNode->AddChild(skyTexNode);
	skyTexNode->AddChild(skySphere);

	skyTNode->Scale(60*AUtoMM, 60*AUtoMM, 60*AUtoMM);

	// -------------------------------------------------------------------------------------
	// Main (lighting) shader instantiation
	LightingShaderNode* lightingShader = new LightingShaderNode;
	if (!lightingShader->Create("FragmentLighting.vert", "FragmentLighting.frag") ||
		!lightingShader->GetLocations())
		exit(-1);

	int lightingPositionLoc = lightingShader->GetPositionLoc();
	int lightingNormalLoc = lightingShader->GetNormalLoc();
	int lightingTextureCoordLoc = lightingShader->GetTextureCoordLoc();

	// -------------------------------------------------------------------------------------
	// orbit path shader

	LineShaderNode* orbitPathShader = new LineShaderNode;
	if (!orbitPathShader->Create("lines.vert", "lines.frag") ||
		!orbitPathShader->GetLocations())
		exit(-1);

	int linePositionLoc = orbitPathShader->GetPositionLoc();

	sceneTNode->AddChild(skyShader);
	
	sceneTNode->AddChild(lightingShader);
	sceneTNode->AddChild(orbitPathShader);

	// -------------------------------------------------------------------------------------
	// Lighting setup

	// lighting node representing the Sun
	LightNode* sunlight = new LightNode(lightingShader);
	lightingShader->AddChild(sunlight);

	HPoint3 position1(0,0,0,1);
	Color4 ambient1(0.2f, 0.2f, 0.2f, 1);
	Color4 diffuse1(1, 0.95f, 0.91f, 1);
	Color4 specular1(1, 0.95f, 0.91f, 1);
	sunlight->addLight(position1, ambient1, diffuse1, specular1, 1, 0, 0);
	sunlight->setGlobalAmbient(Color4(0.01f,0.01f,0.01f,1));

	// -------------------------------------------------------------------------------------
	// the Sun model
	PresentationNode* sunPNode = new PresentationNode;
	TransformNode* sunTNode = new TransformNode;
	SunNode* sunNode = new SunNode(jsondb->getOrbitData("sun"));
	//PlanetNode* sunPlanetNode = new PlanetNode(jsondb->getOrbitData("sun")); // special case
	//TextureNode* sunTextureNode = new TextureNode("sun.jpg");
	StackSphere* sunSphere = new StackSphere(100,100, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

	// The color temperature of sunlight is approx 5900K (per wikipedia)
	// Using this page: http://academo.org/demos/colour-temperature-relationship/
	// we convert the color temp to RGB
	// This is the color being emitted from the sun's surface
	sunPNode->SetMaterialEmission(Color4(1, 0.95f, 0.91f, 1));

	orbitNodes["sun"] = sunNode;

	sunlight->AddChild(sunPNode);
	sunPNode->AddChild(sunTNode);
	sunTNode->AddChild(sunSphere);

	// -------------------------------------------------------------------------------------
	// camera setup
	// needs to happen after Sun setup
	defaultCameraSetup();

	// Radius of Sun = 696342 km
	float sunRadius = 696342 / globalScaleFac;
	sunTNode->Scale(sunRadius*2, sunRadius*2, sunRadius*2);

	// some additional sun setup
	//validObjNames.push_back("sun");
	//objPositions["sun"] = Point3(0,0,0);

	// -------------------------------------------------------------------------------------
	// Setup of all other objects in system

	// names of all objects in JSON file
	validObjNames = jsondb->getObjects();

	// iterates over all sections of the JSON file to create
	// the objects in the system

	std::cout << "Loading scene, please standby..." << std::endl;

	for (std::vector<std::string>::iterator it = validObjNames.begin(); 
		it != validObjNames.end(); ++it)

	{
		std::cout << "Loading object: " << *it << std::endl;

		if (*it == "apollo" || *it == "sun") // skip, special cases
				continue;

		// Scenegraph structure at this point
		// shader -> Presentation -> OrbitNode -> PlanetNode -> TextureNode -> StackSphere
		//                                     \
		//                            for moons -> OrbitNode -> PlanetNode -> TextureNode -> StackSphere
		//										-> LineColorNode -> OrbitPathNode       
		//										\
		//                            for rings  -> PresentationNode -> TextureNode -> Washer

		if (jsondb->getOrbitData(*it).parent == "sun") {
			// these are planets (satellites of the Sun)

			// Orbit paths as lines
			LineColorNode* planetOrbitLineColorNode = new LineColorNode(Color4(0,1,0,1));
			OrbitPathNode* planetOrbitPathNode = new OrbitPathNode(jsondb->getOrbitData(*it), linePositionLoc);
			orbitPathList[*it] = planetOrbitPathNode;

			// add to scenegraph
			orbitPathShader->AddChild(planetOrbitLineColorNode);
			planetOrbitLineColorNode->AddChild(planetOrbitPathNode);

			// create planet nodes
			PresentationNode* planetPNode = new PresentationNode;
			OrbitNode* planetOrbitNode = new OrbitNode(jsondb->getOrbitData(*it));
			PlanetNode* planetNode = new PlanetNode(jsondb->getOrbitData(*it));
			// we only have a specular map for earth right now (third argument)
			// also, normal mapping is disabled since it doesn't work right yet (second argument)
			TextureNode* planetTexNode;
			if (*it == "earth") {
				planetTexNode = new TextureNode(*it + ".jpg", "none", *it + ".jpg");
			} else {
				planetTexNode = new TextureNode(*it + ".jpg", "none", "none");
			}
			StackSphere* planetSphere = new StackSphere(100,100, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

			// modify material properties
			planetPNode->SetMaterialAmbient(Color4(1.0f, 1.0f, 1.0f, 1));
			planetPNode->SetMaterialDiffuse(Color4(1,1,1, 1));
			planetPNode->SetMaterialSpecular(Color4(0.7f, 0.7f,0.7f, 1));
			planetPNode->SetMaterialShininess(25);

			// add to global orbitnode list
			orbitNodes[*it] = planetOrbitNode;

			// add to scenegraph
			sunlight->AddChild(planetPNode);
			planetPNode->AddChild(planetOrbitNode);
			planetOrbitNode->AddChild(planetNode);
			planetNode->AddChild(planetTexNode);		
			planetTexNode->AddChild(planetSphere);
		} else { // satellites of planets
			
			// create orbital path from line loop
			LineColorNode* moonLineCNode = new LineColorNode(Color4(1,0,0,1));
			SatelliteNode* moonPathTNode = new SatelliteNode(jsondb->getOrbitData(*it).parent);
			OrbitPathNode* moonPathNode = new OrbitPathNode(jsondb->getOrbitData(*it), linePositionLoc);
			
			// add to global list of orbit path nodes
			orbitPathList[*it] = moonPathNode;

			// add to scenegraph
			orbitPathShader->AddChild(moonLineCNode);
			moonLineCNode->AddChild(moonPathTNode);
			moonPathTNode->AddChild(moonPathNode);

			// create satellite nodes
			PresentationNode* moonPNode = new PresentationNode;
			//SatelliteNode* moonOrbitTNode = new SatelliteNode(jsondb->getOrbitData(*it).parent);
			OrbitNode* moonOrbitNode = new OrbitNode(jsondb->getOrbitData(*it));
			PlanetNode* moonPlanetNode = new PlanetNode(jsondb->getOrbitData(*it));
			
			// no normal or specular maps
			TextureNode* moonTexNode = new TextureNode(*it + ".jpg", "none", "none");
			StackSphere* moonSphere = new StackSphere(100,100, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

			// add to global list of orbit nodes
			orbitNodes[*it] = moonOrbitNode;
		
			// set material properties
			moonPNode->SetMaterialAmbient(Color4(1.0f, 1.0f, 1.0f, 1));
			moonPNode->SetMaterialDiffuse(Color4(1,1,1, 1));
			moonPNode->SetMaterialSpecular(Color4(0.7f, 0.7f,0.7f, 1));
			moonPNode->SetMaterialShininess(25);

			// add to scenegraph
			orbitNodes[jsondb->getOrbitData(*it).parent]->AddChild(moonPNode);
			moonPNode->AddChild(moonOrbitNode);
			moonOrbitNode->AddChild(moonPlanetNode);
			moonPlanetNode->AddChild(moonTexNode);
			moonTexNode->AddChild(moonSphere);

		}
	}

	// -------------------------------------------------------------------------------------
	// Special case objects to load

	// rings of Saturn
	std::cout << "Loading object: saturn-ring" << std::endl;

	// create nodes
	PresentationNode* saturnRingPNode = new PresentationNode;
	TransformNode* saturnRingTNode = new TransformNode;
	TextureNode* saturnRingTexNode = new TextureNode("saturn-ring2.png", "none", "none");
	Washer* saturnRingNode = new Washer(1.12f, 2.49f, 50, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

	// set material properties
	saturnRingPNode->SetMaterialAmbient(Color4(0.6f, 0.6f, 0.6f, 1));
	saturnRingPNode->SetMaterialDiffuse(Color4(0.6f, 0.6f, 0.6f, 1));
	saturnRingPNode->SetMaterialSpecular(Color4(0.6f, 0.6f, 0.6f,1));
	saturnRingPNode->SetMaterialShininess(5);

	// add to scenegraph
	orbitNodes["saturn"]->AddChild(saturnRingPNode);
	saturnRingPNode->AddChild(saturnRingTNode);
	saturnRingTNode->AddChild(saturnRingTexNode);
	saturnRingTexNode->AddChild(saturnRingNode);

	saturnRingTNode->Rotate(orbitNodes["saturn"]->GetOE().obliquity,1,0,0);
	float rS = (orbitNodes["saturn"]->GetOE().radius) / globalScaleFac;
	saturnRingTNode->Scale(rS,rS,rS);
	

	// -------------------------------------------------------------------------------------
	// rings of Uranus
	std::cout << "Loading object: uranus-ring" << std::endl;

	// create nodes
	PresentationNode* uranusRingPNode = new PresentationNode;
	TransformNode* uranusRingTNode = new TransformNode;
	TextureNode* uranusRingTexNode = new TextureNode("uranus-ring2.png", "none", "none");
	Washer* uranusRingNode = new Washer(1.49f, 3.86f, 50, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

	// set material properties
	uranusRingPNode->SetMaterialAmbient(Color4(0.6f, 0.6f, 0.6f, 1));
	uranusRingPNode->SetMaterialDiffuse(Color4(0.6f, 0.6f, 0.6f, 1));
	uranusRingPNode->SetMaterialSpecular(Color4(0.6f, 0.6f, 0.6f,1));
	uranusRingPNode->SetMaterialShininess(5);

	// add to scenegraph
	orbitNodes["uranus"]->AddChild(uranusRingPNode);
	uranusRingPNode->AddChild(uranusRingTNode);
	uranusRingTNode->AddChild(uranusRingTexNode);
	uranusRingTexNode->AddChild(uranusRingNode);

	uranusRingTNode->Rotate(orbitNodes["uranus"]->GetOE().obliquity,1,0,0);
	float rU = (orbitNodes["uranus"]->GetOE().radius) / globalScaleFac;
	uranusRingTNode->Scale(rU,rU,rU);

	// -------------------------------------------------------------------------------------
	// rings of Jupiter
	std::cout << "Loading object: jupiter-ring" << std::endl;

	// create nodes
	PresentationNode* jupiterRingPNode = new PresentationNode;
	TransformNode* jupiterRingTNode = new TransformNode;
	TextureNode* jupiterRingTexNode = new TextureNode("jupiter-ring.png", "none", "none");
	Washer* jupiterRingNode = new Washer(1.49f, 3.86f, 50, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);

	// set material properties
	jupiterRingPNode->SetMaterialAmbient(Color4(0.6f, 0.6f, 0.6f, 1));
	jupiterRingPNode->SetMaterialDiffuse(Color4(1,1,1, 1));
	jupiterRingPNode->SetMaterialSpecular(Color4(1,1,1,1));
	jupiterRingPNode->SetMaterialShininess(5);

	// add to scenegraph
	orbitNodes["jupiter"]->AddChild(jupiterRingPNode);
	jupiterRingPNode->AddChild(jupiterRingTNode);
	jupiterRingTNode->AddChild(jupiterRingTexNode);
	jupiterRingTexNode->AddChild(jupiterRingNode);

	jupiterRingTNode->Rotate(orbitNodes["jupiter"]->GetOE().obliquity,1,0,0);
	float rJ = (orbitNodes["jupiter"]->GetOE().radius) / globalScaleFac;
	jupiterRingTNode->Scale(rJ,rJ,rJ);

	// -------------------------------------------------------------------------------------
	// Apollo
	std::cout << "Loading object: apollo" << std::endl;

	// create nodes
	PresentationNode* apolloPNode = new PresentationNode;
	SatelliteNode* apolloOrbitTNode = new SatelliteNode("earth");
	OrbitNode* apolloOrbitNode = new OrbitNode(jsondb->getOrbitData("apollo"));
	PlanetNode* apolloPlanetNode = new PlanetNode(jsondb->getOrbitData("apollo"));
	TextureNode* apolloTexNode = new TextureNode("apollo.jpg", "none", "none");
	RotatedSolid* apolloSolid = new RotatedSolid(40, lightingPositionLoc, lightingNormalLoc, lightingTextureCoordLoc);
	
	// add to global orbit node list
	orbitNodes["apollo"] = apolloOrbitNode;

	// set material properties
	apolloPNode->SetMaterialAmbient(Color4(0.6f, 0.6f, 0.6f, 1));
	apolloPNode->SetMaterialDiffuse(Color4(0.6f, 0.6f, 0.6f, 1));
	apolloPNode->SetMaterialSpecular(Color4(1,1,1,1));
	apolloPNode->SetMaterialShininess(5);

	// add to scenegraph
	sunlight->AddChild(apolloPNode);
	apolloPNode->AddChild(apolloOrbitTNode);
	apolloOrbitTNode->AddChild(apolloOrbitNode);
	apolloOrbitNode->AddChild(apolloPlanetNode);
	apolloPlanetNode->AddChild(apolloTexNode);
	apolloTexNode->AddChild(apolloSolid);

	apolloPlanetNode->Translate(0,0,-100);

	// create apollo orbital path
	LineColorNode* apolloLineCNode = new LineColorNode(Color4(1,0,0,1));
	SatelliteNode* apolloPathTNode = new SatelliteNode("earth");
	OrbitPathNode* apolloPathNode = new OrbitPathNode(jsondb->getOrbitData("apollo"), linePositionLoc);
	
	// add to global orbit path list
	orbitPathList["apollo"] = apolloPathNode;

	// add to scenegraph
	orbitPathShader->AddChild(apolloLineCNode);
	apolloLineCNode->AddChild(apolloPathTNode);
	apolloPathTNode->AddChild(apolloPathNode);

	
}

void cameraLookAtObject(std::string name)
{
	std::cout << "Camera centered on " << name << std::endl;

	myCamera->setCameraMode(CENTERED);
	myCamera->setCenter(orbitNodes[name]);
	myCamera->setArcR(5);
}

void slideHandler(sf::Event::KeyEvent ev, float x, float y, float z)
{
	float moveScaleFac;
	float slideFac;

	if (ev.shift)
		moveScaleFac = 5;
	else if (ev.alt)
		moveScaleFac = 0.5f;
	else
		moveScaleFac = 0.1f;

	if (myCamera->getCameraMode() == FREE) {
		slideFac = moveScaleFac * AUtoMM;
		myCamera->Slide(x * slideFac, y * slideFac, z * slideFac);
	} else if (myCamera->getCameraMode() == CENTERED && (ev.code == sf::Keyboard::E || ev.code == sf::Keyboard::D)) {
		myCamera->setArcR(z*moveScaleFac);
	}
}

void rotateHandler(float pitch, float roll, float yaw)
{
	int moveScaleFac = 5;
	if (myCamera->getCameraMode() == FREE) {
		myCamera->Pitch(pitch * moveScaleFac);
		myCamera->Heading(yaw * moveScaleFac);
		myCamera->Roll(roll * moveScaleFac);
	} else if (myCamera->getCameraMode() == CENTERED) {
		myCamera->setArcY(pitch*moveScaleFac);
		myCamera->setArcX(yaw*moveScaleFac);
	}
}

void commandHandler()
{
	// parse command line
	std::string goCmd = "go ";
	if (commandBuffer.compare(0,3,goCmd) == 0) {
		std::string obj = commandBuffer.substr(3,std::string::npos);

		// Make sure the text that remains is a valid object name
		bool found = false;
		for (std::vector<std::string>::iterator it = validObjNames.begin(); it != validObjNames.end(); ++it)
			if (*it == obj)
				found = true;
	
		// If not, error out
		if (!found) {
			std::cout << "\nSorry, " << obj << " is not a valid object name." << std::endl;
		} else { // we're valid
			cameraLookAtObject(obj);
		}
	}

	// clear buffer
	commandBuffer = "";
	captureText = false;
}

// Key event handler
void keypressHandler(sf::Event::KeyEvent keyEvent)
{
	switch (keyEvent.code)
	{
		case sf::Keyboard::Escape:
			window.close();
			break;
		case sf::Keyboard::I:
			if (!captureText) {
				defaultCameraSetup();
				sceneTNode->LoadIdentity();
			}
			break;
		case sf::Keyboard::E: // translate forward
			if (!captureText)
				slideHandler(keyEvent, 0,0,-1);
			break;
		case sf::Keyboard::D: // translate backwards
			if (!captureText)
				slideHandler(keyEvent, 0,0,1);
			break;
		case sf::Keyboard::S: // translate left
			if (!captureText)
				slideHandler(keyEvent, -1,0,0);
			break;
		case sf::Keyboard::F: // translate right
			if (!captureText)
				slideHandler(keyEvent, 1,0,0);
			break;
		case sf::Keyboard::T: // translate up
			if (!captureText)
				slideHandler(keyEvent, 0,1,0);
			break;
		case sf::Keyboard::G: // translate down
			if (!captureText)
				slideHandler(keyEvent, 0,-1,0);
			break;
		case sf::Keyboard::W: // roll left
			if (!captureText)
				rotateHandler(0,-1,0);
			break;
		case sf::Keyboard::R: // roll right
			if (!captureText)
				rotateHandler(0,1,0);
			break;
		case sf::Keyboard::Q: // pitch down
			if (!captureText)
				rotateHandler(-1,0,0);
			break;
		case sf::Keyboard::A: // pitch up
			if (!captureText)
				rotateHandler(1,0,0);
			break;
		case sf::Keyboard::Z: // yaw (heading) right
			if (!captureText)
				rotateHandler(0,0,1);
			break;
		case sf::Keyboard::X: // yaw (heading) left
			if (!captureText)
				rotateHandler(0,0,-1);
			break;
		case sf::Keyboard::F5:
			std::cout << "\nCommand mode activated." << std::endl;
			captureText = true;
			break;
		case sf::Keyboard::Return:
			commandHandler();
			break;
		case sf::Keyboard::Num1:
			if (!captureText) 
				cameraLookAtObject("mercury");
			break;
		case sf::Keyboard::Num2:
			if (!captureText)
				cameraLookAtObject("venus");
			break;
		case sf::Keyboard::Num3:
			if (!captureText) {
				if (keyEvent.shift == true)
					cameraLookAtObject("luna");
				else
					cameraLookAtObject("earth");
			}
			break;
		case sf::Keyboard::Num4:
			if (!captureText)
				cameraLookAtObject("mars");
			break;
		case sf::Keyboard::Num5:
			if (!captureText)
				cameraLookAtObject("jupiter");
			break;
		case sf::Keyboard::Num6:
			if (!captureText)
				cameraLookAtObject("saturn");
			break;
		case sf::Keyboard::Num7:
			if (!captureText)
				cameraLookAtObject("uranus");
			break;
		case sf::Keyboard::Num8:
			if (!captureText)
				cameraLookAtObject("neptune");
			break;
		case sf::Keyboard::Num9:
			if (!captureText)
				cameraLookAtObject("pluto");
			break;
		case sf::Keyboard::Dash:
			if (!captureText)
				cameraLookAtObject("apollo");
			break;
		case sf::Keyboard::O:
			if (!captureText)
				orbits_on = !orbits_on;
			break;
		case sf::Keyboard::Num0:
			// switches to top down view
			if (!captureText) {
				myCamera->setCameraMode(CENTERED);
				myCamera->setCenter(orbitNodes["sun"]);
				myCamera->SetPosition(Point3(0,5*AUtoMM,5*AUtoMM));
				myCamera->SetLookAtPt(Point3(0,0,0));
				myCamera->SetViewUp(Vector3(0,0,1));
			}
			break;
		case sf::Keyboard::M:
			// toggles camera mode
			if (!captureText) {
				if (myCamera->getCameraMode() == FREE)
					myCamera->setCameraMode(CENTERED);
				else if (myCamera->getCameraMode() == CENTERED) {
					myCamera->setCameraMode(FREE);
					myCamera->setCenter(orbitNodes["sun"]);
				}
			}
			break;
		default:
			break;
	}
}


void mouseHandler()
{
	int moveX = sf::Mouse::getPosition(window).x - currentWidth/2;
	int moveY = sf::Mouse::getPosition(window).y - currentHeight/2;

	float scale = 0.75;

	if (moveX < 0) { // yaw left
		myCamera->Heading(scale);
	} else if (moveX > 0) { // yaw right
		myCamera->Heading(-scale);
	}

	if (moveY > 0) { // pitch up
		myCamera->Pitch(-scale);
	} else if (moveY < 0) { // pitch down
		myCamera->Pitch(scale);
	}

	sf::Mouse::setPosition(sf::Vector2i(currentWidth/2, currentHeight/2), window);
}

void textEnteredHandler(sf::Event::TextEvent event)
{
	commandBuffer += (char)event.unicode;
	std::cout << (char)event.unicode;
}

void printDirections()
{
	// needs to be called after object loading is complete (ConstructScene)
	assert(orbitNodes.size() != 0);

	std::cout	<< std::endl
				<< "--------------------------------------------------------------------" << std::endl
				<< "Solar System Simulator - James Curbo" << std::endl
				<< "Final Project for JHU EP EN.605.467.31 - Computer Graphics" << std::endl
				<< "--------------------------------------------------------------------" << std::endl
				<< "Movement controls:              Rotation controls:" << std::endl
				<< "e: Forward                      q: Pitch down 5 deg" << std::endl
				<< "d: Backwards                    a: Pitch up 5 deg" << std::endl
				<< "s: Translate (strafe) left      z: Yaw left 5 deg" << std::endl
				<< "f: Translate (strafe) right     x: Yaw right 5 deg" << std::endl
				<< "t: Translate up                 w: Roll left 5 deg" << std::endl
				<< "g: Translate down               r: Roll right 5 deg" << std::endl
				<< "e/d/s/f/t/g can be modified with alt (faster) or shift (even faster)" << std::endl
				<< "--------------------------------------------------------------------" << std::endl
				<< "Other commands:                         Hotkeys:" << std::endl
				<< "i: Reset camera to default              1: Mercury" << std::endl
				<< "m: Toggle free/centered camera mode     2: Venus" << std::endl
				<< "o: Enable/disable orbit path lines      3: Earth" << std::endl
				<< "                                        shift-3: Luna (the Moon)" << std::endl
				<< "F5: Enter command mode                  4: Mars" << std::endl
				<< "    (type \"go <objectname><Enter>\"    5: Jupiter" << std::endl
				<< "     to jump directly to that           6: Saturn" << std::endl
				<< "     object)                            7: Uranus" << std::endl
				<< "                                        8: Neptune" << std::endl
				<< "                                        9: Pluto" << std::endl
				<< "                                        0: Angled-down view from above Sun" << std::endl
				<< "                                        - (dash): Apollo command/service module" << std::endl
				<< "                                        = (equals): " << std::endl
				<< "--------------------------------------------------------------------" << std::endl
				<< "Current list of objects in system:" << std::endl;
				
				for (std::map<std::string, OrbitNode*>::iterator it = orbitNodes.begin();
					 it != orbitNodes.end(); ++it)
				{
					std::cout << it->second->GetOE().name << std::endl;
				}
}

int main()
{
	// initial settings for OpenGL context
	sf::ContextSettings settings;
	settings.depthBits = 32;
	settings.antialiasingLevel = 4;

	// create the initial window and its properties
	window.create(sf::VideoMode(currentWidth,currentHeight), "Graphics Final Project - James Curbo",
		sf::Style::Default, settings);
	window.setVerticalSyncEnabled(true);

	// GLEW init (happens after window creation because it needs an OpenGL context)
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		std::cerr << "Error: failed to initialize OpenGL:" << glewGetErrorString(err) << std::endl;
		exit(1);
	}
	if (!GLEW_VERSION_3_2)
	{
		std::cerr << "OpenGL 3.2 is not supported" << std::endl;
		exit(1);
	}
	std::cout << "OpenGL " << glGetString(GL_VERSION) 
		<< ", GLSL " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// OpenGL init stuff
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_MULTISAMPLE);

	glEnable (GL_BLEND); 
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	// Initialize our data sources

	// JSON data source for orbit data
	jsondb = new JSONImporter();

	// Construct the scene here
	ConstructScene();

	// Print instructions
	printDirections();

	// initial mouse position
	window.setMouseCursorVisible(false);
	sf::Mouse::setPosition(sf::Vector2i(currentWidth/2, currentHeight/2), window);

	// SFML event handling and display loop for main window
	// Basically the same as setting the GLUT callbacks (display, etc) and calling 
	// glutMainLoop() except SFML handles most things via its own Events
    while (window.isOpen())
    {
		// Clear window
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// draw scene
		mySceneState.Init();
		sceneRoot->Draw(mySceneState);

		//mouseHandler();

		// display current frame
        window.display();
		
		// Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
			// Main event handler
			switch(event.type)
			{
				// Close button pressed
				case sf::Event::Closed:
					window.close();
					break;
				// Key pressed
				case sf::Event::KeyPressed:
					keypressHandler(event.key);
					break;
				// Window resize
				case sf::Event::Resized:
					currentWidth = event.size.width;
					currentHeight = event.size.height;

					// Reset the viewport
					glViewport(0, 0, currentWidth, currentHeight);

					// Reset the perspective projection to reflect the change of aspect ratio 
					myCamera->ChangeAspectRatio((float)currentWidth / (float)currentHeight);

					break;
				// Text Entered
				case sf::Event::TextEntered:
					if (event.text.unicode < 128 && captureText)
						textEnteredHandler(event.text);
					break;
				// Mouse wheel
				case sf::Event::MouseWheelMoved:
					break;
				// Mouse button pressed
				case sf::Event::MouseButtonPressed:
					break;
				// Mouse button released
				case sf::Event::MouseButtonReleased:
					break;
				// Mouse moved
				case sf::Event::MouseMoved:
					break;			
				default:
					break;
			}
        }

		// update all values in the scene
		sceneRoot->Update(mySceneState);

	}

    return 0;
}