#pragma once

#include <geometry\geometry.h>
#include <Scene\Scene.h>

#include <GL\glew.h>

#include <SFML\Graphics.hpp>

#include <SOIL.h>

class TextureNode : public GeometryNode
{
public: 
	TextureNode(std::string imageTex, std::string normalTex, std::string specTex)
	{
		// texture handles for image, bump, alpha
		texture_handle = 0;
		texture_handle_normal = 0;
		texture_handle_spec = 0;

		// load image texture
		std::cout << "Loading image texture " << imageTex << std::endl;
		std::string filenameImage = "data/image/" + imageTex;

		texture_handle = SOIL_load_OGL_texture(filenameImage.c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_MIPMAPS);

		if( 0 == texture_handle )
		{
			std::cerr << "SOIL loading error for image texture: " << imageTex << ": " << SOIL_last_result() << std::endl;
		}

		// load normal map texture (if it exists)

		if (normalTex != "none") {
			normalMappingEnabled = true;
			std::cout << "Loading (optional) normal map texture: " << normalTex << std::endl;
			std::string filenameTex = "data/normal/" + normalTex;
			texture_handle_normal = SOIL_load_OGL_texture(filenameTex.c_str(),
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				SOIL_FLAG_MIPMAPS);

			if( 0 == texture_handle_normal )
			{
				std::cerr << "SOIL loading error for normal map texture " << normalTex << ": " << SOIL_last_result() << std::endl;
			}
		} else {
			normalMappingEnabled = false;
		}



		if (specTex != "none") {
			specularMappingEnabled = true;
			std::cout << "Loading (optional) specular map texture: " << specTex << std::endl;
			std::string filenameSpec = "data/spec/" + specTex;
			texture_handle_spec = SOIL_load_OGL_texture(filenameSpec.c_str(),
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				SOIL_FLAG_MIPMAPS);

			if( 0 == texture_handle_spec )
			{
				std::cerr << "SOIL loading error for specular map texture " << specTex << ": " << SOIL_last_result() << std::endl;
			}
		} else {
			specularMappingEnabled = false;
		}

		// Enable repeating of texture coordinates
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Enable mipmaps and anisotropy
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);
	}

	~TextureNode()
	{
		glDeleteTextures(1, &texture_handle);
		glDeleteTextures(1, &texture_handle_normal);
		glDeleteTextures(1, &texture_handle_spec);
	}

	void Draw(SceneState &sceneState)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_handle);
		glUniform1i(sceneState.m_textureDataLoc, 0);

		if (normalMappingEnabled) {
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, texture_handle_normal);
			glUniform1i(sceneState.m_textureBumpLoc, 1);
		}

		if (specularMappingEnabled) {
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, texture_handle_spec);
			glUniform1i(sceneState.m_textureSpecLoc, 2);
		}
	
		SceneNode::Draw(sceneState);
	}

private:
	GLuint texture_handle;
	GLuint texture_handle_normal;
	GLuint texture_handle_spec;

	bool normalMappingEnabled;
	bool specularMappingEnabled;

};