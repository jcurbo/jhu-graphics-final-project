#pragma once

#include <cmath>

#include "Scene\Scene.h"

#include "Orbit.h"

extern std::map<std::string, Point3> objPositions;
extern const float globalScaleFac;

class OrbitNode : public TransformNode
{
public:
	OrbitNode(OrbitalElements orbit)
	{
		oe = orbit;

		// calculate the beginning position of the orbit
		currentPosition = calcHelioCoords(oe.semiMajorAxis,
											oe.ecc,
											oe.longAscNode,
											oe.argPeri,
											oe.incl,
											oe.curTrueAnomaly);
		//printf("init position: %0.2f %0.2f %0.2f\n", currentPosition.x, currentPosition.y, currentPosition.z);
	}

	virtual void Update(SceneState& sceneState)
	{
		// compute new position by moving us one frame along the orbit
		updateTrueAnomaly();
		currentPosition = calcHelioCoords(oe.semiMajorAxis,
											oe.ecc,
											oe.longAscNode,
											oe.argPeri,
											oe.incl,
											oe.curTrueAnomaly);

		//printf("new position: %8.8f %8.8f %8.8f\n", currentPosition.x, currentPosition.y, currentPosition.z);

		objPositions[oe.name] = currentPosition;

		// update matrix
		m_matrix.SetIdentity();
		m_matrix.Translate(currentPosition.x, currentPosition.y, currentPosition.z);

		//float r = ((float)oe.radius) / globalScaleFac;
		//m_matrix.Scale(r, r, r);

		SceneNode::Update(sceneState);
	}

	Point3 getCurrentPosition()
	{
		return currentPosition;
	}

	void updateTrueAnomaly()
	{
		// the true anomaly is the angle between the argument of perapsis
		// and the current position of a body in orbit.
		// To update it, we need to use the mean motion of the body,
		// which is a number expressed in degs/day in my source.
		// we need to take degs/day and get to degs/frame.
		// we do this by building a factor up from secs per day times
		// the framerate (which we'll assume is 60 since we're locked to
		// vsync)
		double fac = (24 * 60 * 60) * 60; // sped up 100x (assuming 60 fps)

		double newTrueAnomaly = fmod(oe.curTrueAnomaly + (oe.meanMotion / fac) * 1000, 360);

		oe.curTrueAnomaly = newTrueAnomaly;
	}

	OrbitalElements GetOE()
	{
		return oe;
	}

protected:
	OrbitalElements oe;
	Point3 currentPosition;
	//Point3 parentPos;

	OrbitNode() { }
};

