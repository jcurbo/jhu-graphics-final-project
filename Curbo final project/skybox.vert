#version 150

// Incoming vertex and normal attributes
in vec3 vertexPosition;		// Vertex position attribute
in vec2 vertexUV;

out vec2 UV;

uniform mat4 pvm;		// Composite projection, view, model matrix

void main()
{
	mat4 newpvm = pvm;
	newpvm[3][0] = 0.0;
	newpvm[3][1] = 0.0;
	newpvm[3][2] = 0.0;
	newpvm[3][3] = 0.0;

	gl_Position = newpvm * vec4(vertexPosition, 1.0);
	UV = vertexUV;

}