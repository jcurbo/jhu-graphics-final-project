#pragma once

#include "geometry\geometry.h"
#include "Scene\scene.h"

#include <vector>

#include "Orbit.h"

// OpenGL includes
#include <GL/glew.h>
#include <GL/GL.h>

extern bool orbits_on;
extern void logmsg(const char *message, ...);

class OrbitPathNode : public GeometryNode
{
public:
	OrbitPathNode(OrbitalElements o, const int positionLoc)
	{
		active = false;
		count = 360 * 10;
		oe = o;

		//orbitPts.reserve(m_segments);

		// the true anomaly is just an angle; we will split the ellipse into
		// m_segments divisions to generate all the points
		float theta = 0;
		while (theta < 360.0f) {
			// using Positions instead of Point3 for doubles
			Point3 p = calcHelioCoords(oe.semiMajorAxis,
										 oe.ecc,
										 oe.longAscNode,
										 oe.argPeri,
										 oe.incl,
										 theta);
			orbitPts.push_back(p);

			theta += 360.0f/count;
			
			//logmsg("positions for %s at angle %0.4f: x %0.4f y %0.4f z %0.4f", oe.name.c_str(), i, p.x, p.y, p.z);
		}


		glGenBuffers(1, &m_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBufferData(GL_ARRAY_BUFFER, orbitPts.size() * sizeof(Point3), (GLvoid*)&orbitPts[0], GL_STATIC_DRAW);

		glGenVertexArrays(1, &m_vao);
		glBindVertexArray(m_vao);

		glEnableVertexAttribArray(positionLoc);
		glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	~OrbitPathNode()
	{
		glDeleteBuffers(1, &m_vbo);
		glDeleteVertexArrays(1, &m_vao);
	}

	void Draw(SceneState& sceneState)
	{
		glBindVertexArray(m_vao);
		glLineWidth(0.1f);
		if (active)
			glDrawArrays(GL_LINE_LOOP, 0, count);
		glBindVertexArray(0);

		SceneNode::Draw(sceneState);
		
	}

	void Update(SceneState& sceneState)
	{
		active = orbits_on;
		SceneNode::Update(sceneState);
	}

	void enable()
	{
		active = true;
	}

	void disable()
	{
		active = false;
	}

private:
	OrbitalElements oe;
	bool active;
	std::vector<Point3> orbitPts;

	unsigned int count;

	GLuint m_vbo;
	GLuint m_vao;

};