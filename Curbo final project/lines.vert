// Simple vertex shader that transforms the incoming vertex position attribute
// to clip coordinates using the orthographic projection matrix provided via
// the ortho uniform variable
#version 150
in vec3 position;   // 2-D vertex position

uniform mat4 pvm;		// Composite projection, view, model matrix

uniform vec4 color;
out vec4 colorV;

void main()
{
	// Convert position to clip coordinates and pass along
	// Note that we need to make the 2D position a 4 component
	// vector so the 4x4 matrix can multiply it
	gl_Position = pvm * vec4(position, 1.0);

	// Brings the color in from the program and outputs to the 
	// fragment shader
	colorV = color;
}
