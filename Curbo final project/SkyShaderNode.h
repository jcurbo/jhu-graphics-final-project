#pragma once

#include "scene/scene.h"
#include <vector>

#include "GL\glew.h"


class SkyShaderNode : public ShaderNode
{
public:
	bool GetLocations()
	{
		m_positionLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "vertexPosition");
		if (m_positionLoc < 0)
		{
			printf("SkyShaderNode: Error getting vertex position location\n");
		return false;
		}
		// Texture stuff
	  m_textureCoordLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "vertexUV");
	  if (m_textureCoordLoc < 0)
      {
         printf("SkyShaderNode: Error getting texture coordinate location\n");
         return false;
      }

	  // Populate matrix uniform locations in scene state
      m_pvmLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "pvm");

	  m_textureDataLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "textureData");


	  return true;
	}

	virtual void Draw(SceneState& sceneState)
	{
      // Enable this program
      m_shaderProgram.Use();

      // Set scene state locations to ones needed for this program
      sceneState.m_positionLoc = m_positionLoc;
      sceneState.m_pvmLoc = m_pvmLoc;
	  sceneState.m_textureCoordLoc = m_textureCoordLoc;
	  sceneState.m_textureDataLoc = m_textureDataLoc;

      // Draw all children
		SceneNode::Draw(sceneState);
	}

	int GetPositionLoc() const
   {
      return m_positionLoc;
   }

	int GetTextureCoordLoc() const
   {
	   return m_textureCoordLoc;
   }

protected:
	GLint m_positionLoc;
	GLint m_pvmLoc;
   GLint m_textureCoordLoc;
   GLint m_textureDataLoc;

};