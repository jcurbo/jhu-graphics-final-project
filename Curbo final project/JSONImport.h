#pragma once

#include "rapidjson\document.h"
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include "OrbitNode.h"

extern const float AUtoKM;
extern const float AUtoMM;

class JSONImporter
{
public:
	// Constructor, pretty much takes care of everything
	JSONImporter()
	{
		std::ifstream jsonfile("data\\data - 9 + moon + apollo.json");
		std::stringstream jsondata;

		// Pull contents of file into string buffer
		if (jsonfile)
			jsondata << jsonfile.rdbuf();
		else {
			std::cerr << "Couldn't read JSON file" << std::endl;
			exit(1);
		}

		jsonfile.close();

		rapidjson::Document d;
		if(d.Parse(jsondata.str().c_str()).HasParseError()) {
			std::cerr << "JSON parse error\n" << std::endl;
			std::cerr << d.Parse(jsondata.str().c_str()).GetParseError() << std::endl;
			exit(1);
		}

		for (rapidjson::SizeType i = 0; i<d.Size(); i++) {
			OrbitalElements oe;

			oe.name = d[i].FindMember("name")->value.GetString();
			oe.id = d[i].FindMember("id")->value.GetInt();
			oe.epoch = d[i].FindMember("epoch")->value.GetDouble();
			oe.ecc = d[i].FindMember("ecc")->value.GetDouble();
			oe.distPeri = d[i].FindMember("distPeri")->value.GetDouble() * AUtoMM;
			oe.incl = d[i].FindMember("incl")->value.GetDouble();
			oe.longAscNode = d[i].FindMember("longAscNode")->value.GetDouble();
			oe.argPeri = d[i].FindMember("argPeri")->value.GetDouble();
			oe.timePeri = d[i].FindMember("timePeri")->value.GetDouble();
			oe.meanMotion = d[i].FindMember("meanMotion")->value.GetDouble();
			oe.meanAnomaly = d[i].FindMember("meanAnomaly")->value.GetDouble();
			oe.trueAnomaly = d[i].FindMember("trueAnomaly")->value.GetDouble();
			oe.semiMajorAxis = d[i].FindMember("semiMajorAxis")->value.GetDouble() * AUtoMM;
			oe.distApo = d[i].FindMember("distApo")->value.GetDouble() * AUtoMM;
			oe.period = d[i].FindMember("period")->value.GetDouble();
			oe.curTrueAnomaly = oe.trueAnomaly;
			oe.radius = d[i].FindMember("radius")->value.GetDouble();
			oe.obliquity = d[i].FindMember("obliquity")->value.GetDouble();
			oe.rotatePeriod = d[i].FindMember("rotatePeriod")->value.GetDouble();
			oe.parent = d[i].FindMember("parent")->value.GetString();
			
			orbits[oe.name] = oe;

			objects.push_back(oe.name);

		}
	}

	OrbitalElements getOrbitData(std::string name)
	{
		OrbitalElements oe;
		if (orbits.find(name) != orbits.end()) {
			oe = orbits[name];
		}
		return oe;
	}

	std::vector<std::string> getObjects()
	{
		return objects;
	}

private:
	// Nested data structure:
	// Map of String:OrbitalElements pairs that each describe an orbit
	std::map<std::string, OrbitalElements> orbits;

	std::vector<std::string> objects;
};