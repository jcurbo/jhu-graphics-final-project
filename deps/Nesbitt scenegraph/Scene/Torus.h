#ifndef __TORUS_H
#define __TORUS_H

#include <assert.h>
#include <geometry\geometry.h>
#include <Scene\Scene.h>

/*
Our basic idea here is that a torus is a cylinder put on its side and with its top
connected to its bottom.  Thus we can reuse the ConicSurface code, and change
how the stacks are generated, realizing each circle is being pushed out from the
center by the ring's radius.  Because we are just stretching the cylinder around,
we do not have to change how the indexs are generated.

Good website for understanding torii: http://paulbourke.net/geometry/torus/
*/

class Torus : public TriSurface
{
public:
	Torus(const float ringRadius, const float tubeRadius, 
		  const unsigned int nSides, const unsigned int nStacks,
          const int positionLoc, const int normalLoc)
	{
		// ringRadius and tubeRadius need to both be positive
		assert(ringRadius > 0);
		assert(tubeRadius > 0);

		// There are nStacks+1 rows in the vertex list
		m_nRows = nStacks + 1;


		// Create a vertex list. Note that normals can be computed based
      // on the difference in bottom and top radius. Note that we 
      // change radius linearly from the bottom radius to top radius
      unsigned int i, j;

	  // angle inside the tube that describes the cross section (v)
      float thetaT = 0.0f;
      float dthetaT = (2.0f * (float)M_PI) / (float)nSides;
      float cosThetaT, sinThetaT;

	  // angle around the ring (u)
	  float thetaR = 0.0f;
	  float dthetaR = (2.0f * (float)M_PI) / (float)nStacks; // number of stacks = number of subdivisions
	  float cosThetaR, sinThetaR;
	 
      VertexAndNormal vtx;

	  Vector3 n;

	  for (i = 0; i < nSides; i++, thetaR += dthetaR)
      {
		 cosThetaR = cosf(thetaR);
		 sinThetaR = sinf(thetaR);

         // Form vertices along the side for this angle increment
         for (j = 0, thetaT = 0; j <= nStacks; j++, thetaT += dthetaT)
         {
			cosThetaT = cosf(thetaT);
			sinThetaT = sinf(thetaT);

			// adapted easily from http://paulbourke.net/geometry/torus/
            vtx.m_vertex.Set(cosThetaR * (ringRadius + tubeRadius * cosThetaT),
							 sinThetaR * (ringRadius + tubeRadius * cosThetaT),
							 tubeRadius * sinThetaT);

			// Create normal from center of 'ring' to just created vertex
			Point3 ringCenter = Point3(ringRadius * cosThetaR, ringRadius * sinThetaR, 0);
			n = vtx.m_vertex - ringCenter;
			n.Normalize();
			vtx.m_normal = n;

            m_vertexList.push_back(vtx);
         }

      }

	  // Copy the first column of vertices
      for (i = 0; i < m_nRows; i++)
         m_vertexList.push_back(m_vertexList[i]);

      // Form triangle face indexes. Note: there are n+1 rows 
		// and n+1 columns.
		m_nCols = nSides + 1;
		for (unsigned int row = 0; row < m_nRows - 1; row++)
		{
			for (unsigned int col = 0; col < m_nCols - 1; col++)
			{
				// Divide each square into 2 triangles - make sure they are ccw.
				// GL_TRIANGLES draws independent triangles for each set of 3 vertices
				m_faceList.push_back(getIndex(row+1, col));
				m_faceList.push_back(getIndex(row, col));
				m_faceList.push_back(getIndex(row, col+1));
				
				m_faceList.push_back(getIndex(row+1, col));
				m_faceList.push_back(getIndex(row, col+1));
				m_faceList.push_back(getIndex(row+1, col+1));
			}
		}

		// Connect the last column with the first
      for (unsigned int row = 0; row < m_nRows - 1; row++)
      {
			m_faceList.push_back(getIndex(row+1, m_nCols - 1));
			m_faceList.push_back(getIndex(row,   m_nCols - 1));
			m_faceList.push_back(getIndex(row,   0));
				
			m_faceList.push_back(getIndex(row+1, m_nCols - 1));
			m_faceList.push_back(getIndex(row,   0));
			m_faceList.push_back(getIndex(row+1, 0));
      }

      // Create vertex buffers
      CreateVertexBuffers(positionLoc, normalLoc);
}

private:
   unsigned int m_nRows;
   unsigned int m_nCols;

	// Convenience method to get the index into the vertex list given the
	// "row" and "column" of the subdivision/grid
	unsigned int getIndex(unsigned int row, unsigned int col) const
	{
		return (col*m_nRows) + row;
	}

};

#endif