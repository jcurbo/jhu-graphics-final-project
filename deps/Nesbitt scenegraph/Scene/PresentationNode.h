//============================================================================
//	Johns Hopkins University Engineering Programs for Professionals
//	605.467 Computer Graphics and 605.767 Applied Computer Graphics
//	Instructor:	David W. Nesbitt
//
//	Author:	David W. Nesbitt
//	File:    PresentationNode.h
//	Purpose: Scene graph presentation node.
//
//============================================================================

#ifndef __PRESENTATIONNODE_H
#define __PRESENTATIONNODE_H

/**
 * Presentation node. Applies material and texture
 */
class PresentationNode : public SceneNode
{
public:
	/**
	 * Constructor
	 */
	PresentationNode() 
	{
		m_nodeType          = SCENE_PRESENTATION;
		m_materialShininess = 1.0f;

		// Note: color constructors default rgb to 0 and alpha to 1
	}
	
	/**
	 * Destructor
	 */
	~PresentationNode() { }

	/**
	 * Set material ambient reflection coefficient.
	 */
	void SetMaterialAmbient(const Color4& c)
	{
		m_materialAmbient = c;
	}
	
	void SetMaterialDiffuse(const Color4& c)
	{
		m_materialDiffuse = c;
	}
	
	void SetMaterialAmbientAndDiffuse(const Color4& c)
	{
		m_materialAmbient = c;
		m_materialDiffuse = c;
	}
	
	void SetMaterialSpecular(const Color4& c)
	{
		m_materialSpecular = c;
	}
	
	void SetMaterialEmission(const Color4& c)
	{
		m_materialEmission = c;
	}
	
	void SetMaterialShininess(const float s)
	{
		m_materialShininess = s;
	}

	/**
	 * Draw. Simply sets the material properties.
	 */
	void Draw(SceneState& sceneState)
	{
      // Set the material uniform values
      glUniform4fv(sceneState.m_materialAmbientLoc, 1,  &m_materialAmbient.r);
      glUniform4fv(sceneState.m_materialDiffuseLoc, 1,  &m_materialDiffuse.r);
      glUniform4fv(sceneState.m_materialSpecularLoc, 1, &m_materialSpecular.r);
      glUniform4fv(sceneState.m_materialEmissionLoc, 1, &m_materialEmission.r);
      glUniform1f(sceneState.m_materialShininessLoc, m_materialShininess);
  
		// Draw children of this node
		SceneNode::Draw(sceneState);
	}
	
protected:
	Color4       m_materialAmbient;
	Color4       m_materialDiffuse;
	Color4       m_materialSpecular;
	Color4       m_materialEmission;
	GLfloat      m_materialShininess;
};

#endif