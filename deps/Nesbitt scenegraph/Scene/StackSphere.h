#ifndef __STACKSPHERE_H
#define __STACKSPHERE_H

#include <assert.h>
#include <cmath>
#include "geometry\geometry.h"
#include "Scene\Scene.h"

class StackSphere : public TriSurface
{
public:
	StackSphere(const unsigned int nSlices, const unsigned int nStacks,
		const int positionLoc, const int normalLoc, const int textureLoc)
	{
		assert(nSlices > 0);
		assert(nStacks > 0);

		// Creating a sphere from slices and stacks
		// Each stack is a strip of latitude around the sphere

		for (unsigned int i = 0; i <= nStacks; i++)
		{
			float v = i / (float)nStacks;
			float phi = v * (float)M_PI;

			for (unsigned int j = 0; j <= nSlices; j++)
			{
				float u = j / (float)nSlices;
				float theta = u * 2 * (float)M_PI;
				
				float x = sin(theta) * sin(phi);
				float y = cos(theta) * sin(phi);
				float z = cos(phi);

				//printf("u: %f v:%f\n", u, v);

				Point2 texCoord = Point2(1-u, v);
				m_texCoordList.push_back(texCoord);

				VertexAndNormal vtx;
				vtx.m_vertex = Point3(x, y, z);
				vtx.m_normal = Vector3(x, y, z);
				vtx.m_normal.Normalize();

				m_vertexList.push_back(vtx);

				//printf("vtx %d %d: %0.8f %0.8f %0.8f\n", i, j, vtx.m_vertex.x, vtx.m_vertex.y, vtx.m_vertex.z);
				//printf("u: %f, v: %f\n", u, v);

			}

		}

		// facelist construction
		for (unsigned int i = 0; i < nStacks; ++i)
		{
			for (unsigned int j = 0; j < nSlices; ++j)
			{
				int snd = (j * (nStacks + 1)) + i;
				int fst = snd + nStacks + 1;

				m_faceList.push_back(fst);
				m_faceList.push_back(snd);
				m_faceList.push_back(fst + 1);

				m_faceList.push_back(snd);
				m_faceList.push_back(snd + 1);
				m_faceList.push_back(fst + 1);
			}
		}

		CreateVertexBuffers(positionLoc, normalLoc);
		CreateTextureBuffers(textureLoc);
	}

	~StackSphere()
	{
		glDeleteBuffers(1, &m_texBuffer);
	}

private:
	GLuint m_texBuffer;
	
	std::vector<Point2> m_texCoordList;

	void CreateTextureBuffers(const int textureCoordLoc)
	{
		m_texBuffer = 0;

		glGenBuffers(1, &m_texBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_texBuffer);
		glBufferData(GL_ARRAY_BUFFER, m_texCoordList.size() * sizeof(Point2),
					m_texCoordList.data(), GL_STATIC_DRAW);

		glBindVertexArray(m_vao);
		glVertexAttribPointer(textureCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(textureCoordLoc);

		glBindVertexArray(0);
		glBindBuffer( GL_ARRAY_BUFFER, 0 );
	}

};

#endif