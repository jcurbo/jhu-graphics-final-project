#ifndef __LIGHTNODE_H
#define __LIGHTNODE_H

#include <assert.h>
#include <map>

#include "geometry\geometry.h"
#include "Scene\Scene.h"
#include "LightingShaderNode.h"

// struct for describing a light
struct LightSource
{
	// used as index for array in fragment shader
	unsigned int lightID;
	// light position
	HPoint3 position;
	// light colors
	Color4 ambient;
	Color4 diffuse;
	Color4 specular;
	// on or off
	bool enabled;
	// attenuation values
	float constAtt;
	float linAtt;
	float quadAtt;

};

// spotlight info
struct SpotlightSource
{
	// cutoff angle for light
	float spotCutoffAngle;
	// spotlight exponent
	float spotExp;
	// direction of spotlight
	Vector3 spotDirection;
};

class LightNode : public SceneNode
{
public:
	LightNode(LightingShaderNode* shader)
	{
		// just sets the internal pointer to the lighting shader
		lighting = shader;
		// starts off empty
		numLights = 0;
	}

	// this simply sets the global ambient color for the whole scene
	void setGlobalAmbient(Color4 ambientColor)
	{
		globalAmbient = ambientColor;
		lighting->SetGlobalAmbient(ambientColor);
	}


	// Adds a new light
	void addLight(HPoint3 position, Color4 ambient, Color4 diffuse, Color4 specular,
		float constA, float linA, float quadA)
	{
		// generates a lightID
		unsigned int newLightID = numLights;
		numLights += 1;
		//printf("\nadding a light: now at %u lights\n", numLights);

		LightSource newLight;
		lights[newLightID] = newLight;

		updateLight(newLightID, position, ambient, diffuse, specular, constA, linA, quadA);

	}

	// updates a light
	void updateLight(unsigned int lightID, HPoint3 position, Color4 ambient, Color4 diffuse, Color4 specular,
		float constA, float linA, float quadA)
	{
		assert(lightID >= 0);

		lights[lightID].lightID = lightID;
		lights[lightID].position = position;
		lights[lightID].ambient = ambient;
		lights[lightID].diffuse = diffuse;
		lights[lightID].specular = specular;
		lights[lightID].constAtt = constA;
		lights[lightID].linAtt = linA;
		lights[lightID].quadAtt = quadA;

		// calls into the shader to update its values
		lighting->SetLight(lightID, position, ambient, diffuse, specular, constA, linA, quadA);
	}
	
	// function to just update light position for ease of use
	void updateLightPosition(unsigned int lightID, HPoint3 position)
	{
		assert (lightID >= 0);

		//printf("\nupdating light position for lightID %u\n", lightID);
		//printf("  new pos: %0.2f %0.2f %0.2f\n", position.x, position.y, position.z);

		lights[lightID].position = position;
		lighting->SetLight(lightID, position, 
			lights[lightID].ambient, lights[lightID].diffuse, 
			lights[lightID].specular, lights[lightID].constAtt, 
			lights[lightID].linAtt, lights[lightID].quadAtt);
	}

	// updates spotlight info
	void updateSpotlight(float cutoffAngle, float exp, Vector3 dir)
	{
		
		spotlight.spotCutoffAngle = cutoffAngle;
		spotlight.spotExp = exp;
		spotlight.spotDirection = dir;

		// updates spotlight info in shader
		lighting->SetSpotlight(spotlight.spotCutoffAngle,
							   spotlight.spotExp,
							   spotlight.spotDirection);
	}

	// function to just update spotlight direction
	void updateSpotlightDirection(Vector3 dir)
	{
		//printf("updating spotlight direction\n");
		spotlight.spotDirection = dir;
		lighting->SetSpotlight(spotlight.spotCutoffAngle,
							   spotlight.spotExp,
							   spotlight.spotDirection);
	}

	// removes a light from the scene
	void removeLight(unsigned int lightID)
	{
		assert(lightID >= 0);
		
		lights.erase(lightID);
		numLights--;
	}

	// enables a light
	void enableLight(unsigned int lightID)
	{
		assert(lightID >= 0);
		
		lights[lightID].enabled = true;
	}

	// disables a light
	void disableLight(unsigned int lightID)
	{
		assert(lightID >= 0);

		lights[lightID].enabled = false;
	}
	
private:
	unsigned int numLights;
	Color4 globalAmbient;
	LightingShaderNode* lighting;
	std::map<unsigned int, LightSource> lights;
	SpotlightSource spotlight;
};
#endif