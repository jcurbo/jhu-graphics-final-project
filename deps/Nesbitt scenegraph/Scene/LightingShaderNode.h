//============================================================================
//	Johns Hopkins University Engineering Programs for Professionals
//	605.467 Computer Graphics and 605.767 Applied Computer Graphics
//	Instructor:	David W. Nesbitt
//
//	Author:	David W. Nesbitt
//	File:    LineShaderNode.h
//	Purpose:	Derived class to handle an offset line shader and its uniforms and 
//          attribute locations.
//
//============================================================================

#ifndef __LIGHTINGSHADERNODE_H
#define __LIGHTINGSHADERNODE_H

#include <vector>
#include "scene/scene.h"

/**
 * Offset line shader node.
 */
class LightingShaderNode: public ShaderNode
{
public:
   /**
    * Gets uniform and attribute locations.
    */
   bool GetLocations() 
   {
      //checkError("Top of GetLocations");

      m_positionLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "vertexPosition");
      if (m_positionLoc < 0)
      {
         printf("LightingShaderNode: Error getting vertex position location\n");
         return false;
      }
      m_vertexNormalLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "vertexNormal");
      if (m_vertexNormalLoc < 0)
      {
         printf("LightingShaderNode: Error getting vertex normal location\n");
         return false;
      }

      m_globalAmbientLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "globalLightAmbient");
      if (m_globalAmbientLoc < 0)
      {
         printf("LightingShaderNode: Error getting global ambient location\n");
         return false;
      }

	  m_spotCutoffAngleLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "spotCutoffAngle");
      if (m_spotCutoffAngleLoc < 0)
      {
         printf("LightingShaderNode: Error getting spotlight cutoff angle location\n");
         return false;
      }

	  m_spotExpLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "spotExp");
      if (m_spotExpLoc < 0)
      {
         printf("LightingShaderNode: Error getting spotlight exponent location\n");
         return false;
      }

	  m_spotDirLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "spotDir");
      if (m_spotDirLoc < 0)
      {
         printf("LightingShaderNode: Error getting spotlight direction location\n");
         return false;
      }

      // Set the number of lights to 2
      numLights = 1;
      m_numLightsLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "numLights");
      if (m_numLightsLoc < 0)
      {
         printf("LightingShaderNode: Error getting numLightsLoc location\n");
         return false;
      }

	  // Texture stuff
	  m_textureCoordLoc = glGetAttribLocation(m_shaderProgram.GetProgram(), "vertexUV");
	  if (m_textureCoordLoc < 0)
      {
         printf("LightingShaderNode: Error getting texture coordinate location\n");
         return false;
      }

      // Get light uniforms
      char name[128];
      for (int i = 0; i < numLights; i++)
      {
         sprintf(name, "lights[%d].enabled", i);
         m_lights[i].enabled = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
         sprintf(name, "lights[%d].position", i);
         m_lights[i].position = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
         sprintf(name, "lights[%d].ambient", i);
         m_lights[i].ambient = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
         sprintf(name, "lights[%d].diffuse", i);
         m_lights[i].diffuse = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
         sprintf(name, "lights[%d].specular", i);
         m_lights[i].specular = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
		 sprintf(name, "lights[%d].constAtt", i);
		 m_lights[i].constAtt = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
		 sprintf(name, "lights[%d].linAtt", i);
		 m_lights[i].linAtt = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
		 sprintf(name, "lights[%d].quadAtt", i);
		 m_lights[i].quadAtt = glGetUniformLocation(m_shaderProgram.GetProgram(), name);
      }
      
      // Populate matrix uniform locations in scene state
      m_pvmLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "pvm");
      m_modelMatrixLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "modelMatrix");
      m_normalMatrixLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "normalMatrix");
  
      // Populate material uniform locations in scene state 
      m_materialAmbientLoc   = glGetUniformLocation(m_shaderProgram.GetProgram(), "materialAmbient");
      m_materialDiffuseLoc   = glGetUniformLocation(m_shaderProgram.GetProgram(), "materialDiffuse");
      m_materialSpecularLoc  = glGetUniformLocation(m_shaderProgram.GetProgram(), "materialSpecular");
      m_materialEmissionLoc  = glGetUniformLocation(m_shaderProgram.GetProgram(), "materialEmission");
      m_materialShininessLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "materialShininess");

      // Populate camera position uniform location in scene state
      m_cameraPositionLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "cameraPosition");

	  m_textureDataLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "textureData");
	  m_textureBumpLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "textureBumpData");
	  m_textureSpecLoc = glGetUniformLocation(m_shaderProgram.GetProgram(), "textureSpecData");

      return true;
   }

	/**
    * Draw method for this shader - enable the program and set up uniforms
    * and vertex attribute locations
    * @param  sceneState   Current scene state.
	 */
	virtual void Draw(SceneState& sceneState)
	{
      // Enable this program
      m_shaderProgram.Use();

      glUniform1i(m_numLightsLoc, numLights);

      // Set scene state locations to ones needed for this program
      sceneState.m_positionLoc = m_positionLoc;
      sceneState.m_normalLoc = m_vertexNormalLoc;
      sceneState.m_cameraPositionLoc = m_cameraPositionLoc;
      sceneState.m_pvmLoc = m_pvmLoc;
      sceneState.m_modelMatrixLoc = m_modelMatrixLoc;
      sceneState.m_normalMatrixLoc = m_normalMatrixLoc;
      sceneState.m_materialAmbientLoc = m_materialAmbientLoc;
      sceneState.m_materialDiffuseLoc = m_materialDiffuseLoc;
      sceneState.m_materialSpecularLoc = m_materialSpecularLoc;
      sceneState.m_materialEmissionLoc = m_materialEmissionLoc;
      sceneState.m_materialShininessLoc = m_materialShininessLoc;

	  sceneState.m_spotCutoffAngleLoc = m_spotCutoffAngleLoc;
	  sceneState.m_spotExpLoc = m_spotExpLoc;
	  sceneState.m_spotDirLoc = m_spotDirLoc;

	  sceneState.m_textureCoordLoc = m_textureCoordLoc;
	  sceneState.m_textureDataLoc = m_textureDataLoc;
	  

      // Draw all children
		SceneNode::Draw(sceneState);
	}

   /**
    * Set the lighting
    */
   void SetGlobalAmbient(const Color4& globalAmbient) 
   {
      m_shaderProgram.Use();
      glUniform4fv(m_globalAmbientLoc, 1, &globalAmbient.r);
   }


   /**
    * Set light
    */
   void SetLight(const unsigned int n, const HPoint3& position, const Color4& ambient,
                 const Color4& diffuse, const Color4& specular, float constA,
				 float linA, float quadA)
   {
      m_shaderProgram.Use();
      glUniform1i(m_lights[n].enabled, 1);
      glUniform4fv(m_lights[n].position, 1, &position.x);
      glUniform4fv(m_lights[n].ambient, 1, &ambient.r);
      glUniform4fv(m_lights[n].diffuse, 1, &diffuse.r);
      glUniform4fv(m_lights[n].specular, 1, &specular.r);
	  glUniform1f(m_lights[n].constAtt, constA);
	  glUniform1f(m_lights[n].linAtt, linA);
	  glUniform1f(m_lights[n].quadAtt, quadA);

	  /*printf("\nsetting light:\n");
	  printf("  id: %u\n", n);
	  printf("  type: %0.2f\n", position.w);
	  printf("  pos: %0.2f %0.2f %0.2f\n", position.x, position.y, position.z);
	  printf("  amb: %0.2f %0.2f %0.2f\n", ambient.r, ambient.g, ambient.b);
	  printf("  dif: %0.2f %0.2f %0.2f\n", diffuse.r, diffuse.g, diffuse.b);
	  printf("  spe: %0.2f %0.2f %0.2f\n", specular.r, specular.g, specular.b);
	  printf("  constatt: %0.2f\n", consta);
	  printf("  linatt: %0.2f\n", lina);
	  printf("  quadatt: %0.2f\n", quada);*/

   }

   void SetSpotlight(float& cutoffAngle, float exp, Vector3& dir)
   {
	   m_shaderProgram.Use();
	   glUniform1f(m_spotCutoffAngleLoc, cutoffAngle);
	   glUniform1f(m_spotExpLoc, exp);
	   glUniform3fv(m_spotDirLoc, 1, &dir.x);

	   /*printf("\nsetting spotlight:\n");
	   printf("  cutoff angle: %0.2f\n", cutoffAngle);
	   printf("  exponent: %0.2f\n", exp);
	   printf("  dir: %0.2f %0.2f %0.2f\n", dir.x, dir.y, dir.z);*/
   }

   /**
    * Get the location of the vertex position attribute.
    * @return  Returns the vertex position attribute location.
    */
   int GetPositionLoc() const
   {
      return m_positionLoc;
   }

   /**
    * Get the location of the vertex position attribute.
    * @return  Returns the vertex position attribute location.
    */
   int GetNormalLoc() const
   {
      return m_vertexNormalLoc;
   }

   int GetTextureCoordLoc() const
   {
	   return m_textureCoordLoc;
   }

protected:
   // Uniform and attribute locations
   GLint m_positionLoc;
   GLint m_vertexNormalLoc;
   GLint m_pvmLoc;
   GLint m_modelMatrixLoc;
   GLint m_normalMatrixLoc;
   GLint m_materialAmbientLoc;
   GLint m_materialDiffuseLoc;
   GLint m_materialSpecularLoc;
   GLint m_materialEmissionLoc;
   GLint m_materialShininessLoc;
   GLint m_cameraPositionLoc;
   GLint m_globalAmbientLoc;

   GLint m_textureCoordLoc;
   GLint m_textureDataLoc;
   GLint m_textureBumpLoc;
   GLint m_textureSpecLoc;

   GLint m_spotCutoffAngleLoc;
   GLint m_spotExpLoc;
   GLint m_spotDirLoc;

   int numLights;
   GLint m_numLightsLoc;
   LightUniforms m_lights[1];
};

#endif